/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rsControl;
import 'package:flutter/foundation.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:elrepo_lib/repo.dart' as repo;

/// Util function for tests to ensure RS initialization
Future<void> rsInitialization () async {
  print('Initializing RS');
  try {
    await rsControl.initializeRetroShare(() => print('Rs start error!!!'));
  } catch (e){
    print(e);
  }
  print('RS Initialized');
}

Future<void> requestFilePermissions () async {
  print('Requesting permisions');
  assert(await Permission.storage.request().isGranted);
}
final pass = '1234', loc = 'test_location';

Future<void> signup() async{
  final location = await repo.signUp(pass, loc);
  if(defaultTargetPlatform == TargetPlatform.android) {
    // Persist data avoiding crashes
    await repo.login(pass, location);
    await rsControl.RsServiceControlPlatform.instance
        .stopRetroShareExecution();
    await rsControl.RsServiceControlPlatform.instance
        .startRetroshareLoop();
  }
}
/*
 * RetroShare
 * Copyright (C) 2016-2018  Gioacchino Mazzurco <gio@eigenlab.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.altermundi.elrepoio

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent;
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager;
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import androidx.core.app.NotificationCompat
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import io.flutter.embedding.engine.FlutterEngine
import android.os.SystemClock
import android.app.AlarmManager

import org.retroshare.service.RetroShareServiceAndroid as RsService

class RetroShareServiceAndroid : RsService() {

    companion object {
        @JvmStatic
        val ACTION_SHUTDOWN = "SHUTDOWN"
        @JvmStatic
        val ACTION_START = "START"
        @JvmStatic
        val WAKELOCK_TAG = "RetroShareServiceAndroid:Wakelock"
        @JvmStatic
        val CHANNEL_ID = "net.altermundi.elrepoio/retroshare"
        @JvmStatic
        private val TAG = "RetroShareServiceAndroid"
        @JvmStatic
        val EXTRA_NOTIFICATION_IMPORTANCE = "net.altermundi.elrepoio:Importance"
        @JvmStatic
        val EXTRA_NOTIFICATION_TITLE = "net.altermundi.elrepoio:Title"
        @JvmStatic
        val EXTRA_NOTIFICATION_TEXT = "net.altermundi.elrepoio:Text"

        fun start(
            ctx: Context,
            jsonApiPort: Int = DEFAULT_JSON_API_PORT,
            jsonApiBindAddress: String = DEFAULT_JSON_API_BINDING_ADDRESS) {
            Log.d(TAG, "start")

            val JSON_API_PORT_KEY = RsService::class.java.canonicalName + "/JSON_API_PORT_KEY"
            val JSON_API_BIND_ADDRESS_KEY =
                RsService::class.java.canonicalName + "/JSON_API_BIND_ADDRESS_KEY"

            val intent = Intent(ctx, RetroShareServiceAndroid::class.java)
            intent.putExtra(JSON_API_PORT_KEY, jsonApiPort)
            intent.putExtra(JSON_API_BIND_ADDRESS_KEY, jsonApiBindAddress)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ctx!!.startForegroundService(intent)
            } else {
                ctx!!.startService(intent)
            }
        }

        fun stop(ctx: Context) {
            Log.d(TAG, "stop")
            val intent = Intent(ctx, RetroShareServiceAndroid::class.java)
            intent.action = ACTION_SHUTDOWN
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ctx.startForegroundService(intent)
            } else {
                ctx.startService(intent)
            }
        }

    }

    override fun onBind(intent: Intent) : IBinder? {
        return super.onBind(intent);
    }

    @SuppressLint("WakelockTimeout")
    override public fun onCreate() {
        val pm = getApplicationContext().getPackageManager()
        val notificationIntent  =
                pm.getLaunchIntentForPackage(getApplicationContext().getPackageName())
        val pendingIntent  = PendingIntent.getActivity(
                this, 0,
                notificationIntent, 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                    CHANNEL_ID,
                    MainActivity.notificationTitle,
                    MainActivity.notificationImportance ?: NotificationCompat.PRIORITY_DEFAULT).apply {
                description = MainActivity.notificationText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        val imageId = resources.getIdentifier(MainActivity.notificationIconName, MainActivity.notificationIconDefType, packageName)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(MainActivity.notificationTitle)
                .setContentText(MainActivity.notificationText)
                .setSmallIcon(imageId)
                .setContentIntent(pendingIntent)
                .setPriority(MainActivity.notificationImportance ?: NotificationCompat.PRIORITY_DEFAULT)
                .build()

        (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
            newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_TAG).apply {
                setReferenceCounted(false)
                acquire()
            }
        }
        startForeground(1, notification)
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int) : Int {
        if (intent?.action == ACTION_SHUTDOWN) {
            (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_TAG).apply {
                    if (isHeld) {
                        release()
                    }
                }
            }
            stopForeground(true)
            stopSelf()
        } else {
            super.onStartCommand(intent, flags, startId)
        }
        return START_STICKY;
    }

    // Code below is to prevent remove the task from recent apps
    override fun onTaskRemoved(rootIntent: Intent) {
        val restartServiceIntent = Intent(applicationContext, RetroShareServiceAndroid::class.java).also {
            it.setPackage(packageName)
        };
        val restartServicePendingIntent: PendingIntent = PendingIntent.getService(this, 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        applicationContext.getSystemService(Context.ALARM_SERVICE);
        val alarmService: AlarmManager = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager;
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePendingIntent);
    }
}

/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';
import 'dart:io';

import 'package:elRepoIo/ui/main_page/pages/explore_page.dart';
import 'package:elRepoIo/ui/common/posts_teaser.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:elrepo_lib/models.dart' show ParsePostMetadata;
import 'package:http/http.dart' as http;

import 'mock.dart';


void main() {

  /// Some widgets needs to be build inside a MediaQueryWidget
  Widget getMediaQueryWidget(Widget widget) =>
      MediaQuery(
          data: MediaQueryData(),
          child: MaterialApp(home: widget)
      );

  group('A group of tests', () {
    setUp(() {

    });

    // Todo(kon): This test will fall always because the network request did by elrepo-lib
    // Flutter blocks outgoing http requests on flutter tests, so the only way to test is
    // mocking it up with mockito https://flutter.dev/docs/cookbook/testing/unit/mocking.
    // Also I tried deactivating the Http restriction doing https://timm.preetz.name/articles/http-request-flutter-test
    // without exit. Probably the method above is not support by Flutter 2.0
    // So one solution could be to try to disable the restriction or mockup the responses,
    // which will be anoying.
    testWidgets('Test explore screen', (WidgetTester tester) async {
      // Widget testWidget = getMediaQueryWidget(ExploreScreen());
      //
      // await tester.pumpWidget(
      //     testWidget
      // );
      //
      // // Let future builder work
      // await tester.pump();
      //
      // var teaserFinder = find.byType(PostTeaserCard);
      // expect(teaserFinder, findsNothing);
    });

    /// Test the post teaser card
    ///
    /// * Test if content type icon is correct
    /// * test title is shown
    testWidgets('Test postTeaserCard', (WidgetTester tester) async {
      var metadata = MockRsMsgMetaData.exploreScreenMetadata;
      await tester.pumpWidget( getMediaQueryWidget(PostTeaserCard(metadata)));

      final titleFinder = find.text(metadata.postMetadata.title);
      final icon = find.byIcon(
          getContentTypeIcon(metadata.postMetadata.contentType));
      expect(titleFinder, findsOneWidget);
      expect(icon, findsOneWidget);
    });


  });
}

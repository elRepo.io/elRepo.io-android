/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/RsServiceControl/android_config.dart';
import 'package:elRepoIo/RsServiceControl/rscontrol.dart';
import 'package:flutter/services.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

const rsPlatform = MethodChannel(rs.RETROSHARE_CHANNEL_NAME);

/// Inspired by https://github.com/JulianAssmann/flutter_background/ thanks :)
class RsServiceControlAndroid extends RsServiceControlPlatform{

  /// Initializes the plugin.
  /// May request the necessary permissions from the user in order to run in the background.
  ///
  /// Does nothing and returns true if the permissions are already granted.
  /// Returns true, if the user grants the permissions, otherwise false.
  /// May throw a [PlatformException].
  @override
  Future<bool> initialize(
      {FlutterRetroshareServiceAndroidConfig androidConfig =
          const FlutterRetroshareServiceAndroidConfig(), Function startRetroShareErrorCallback}) async {
    isInitialized = await rsPlatform.invokeMethod<bool>('initialize', {
          'android.notificationTitle': androidConfig.notificationTitle,
          'android.notificationText': androidConfig.notificationText,
          'android.notificationImportance': _androidNotificationImportanceToInt(
              androidConfig.notificationImportance),
          'android.notificationIconName': androidConfig.notificationIcon.name,
          'android.notificationIconDefType':
              androidConfig.notificationIcon.defType,
        }) ==
        true;
    // startRsErrorCallback = startRetroShareErrorCallback;
    return isInitialized;
  }

  /// Enables the execution of the flutter app in the background.
  /// You must to call [RsServiceControl.initialize()] before calling this function.
  ///
  /// Returns true if successful, otherwise false.
  /// Throws an [Exception] if the plugin is not initialized by calling [FlutterBackground.initialize()] first.
  /// May throw a [PlatformException].
  @override
  Future<bool> startRetroShareExecution() async {
    if (isInitialized) {
      final success =
          await rsPlatform.invokeMethod<bool>('enableBackgroundExecution');
      isBackgroundExecutionEnabled = true;
      // If can't open background execution launch error starting RetroShare
      if(success != true && startRsErrorCallback != null) startRsErrorCallback();
      return success == true;
    } else {
      throw Exception(
          'RsServiceControl plugin must be initialized before calling enableBackgroundExecution()');
    }
  }

  /// Disables the execution of the flutter app in the background.
  /// You must to call [FlutterBackground.initialize()] before calling this function.
  ///
  /// Returns true if successful, otherwise false.
  /// Throws an [Exception] if the plugin is not initialized by calling [FlutterBackground.initialize()] first.
  /// May throw a [PlatformException].
  Future<bool> _disableBackgroundExecution() async {
    if (isInitialized) {
      final success =
          await rsPlatform.invokeMethod<bool>('disableBackgroundExecution');
      isBackgroundExecutionEnabled = false;
      return success == true;
    } else {
      throw Exception(
          'RsServiceControl plugin must be initialized before calling disableBackgroundExecution()');
    }
  }

  /// Stop RetroShare and disable the sticky notification
  @override
  Future<bool> stopRetroShareExecution() async {
    // Disable restart loop
    disableControlCallbacks();
    try {
      await _disableBackgroundExecution();

      var _rsIsRunning = true;
      for(var x = 0 ; x < 5 && _rsIsRunning ; x++){
        print('Awaiting Rs stop $x');
        await Future.delayed(Duration(milliseconds: 3000));
        _rsIsRunning = await rs.isRetroshareRunning();
      }
      if (_rsIsRunning) {
        throw Exception('The service did not stop after a while');
      }
    } catch (err) {
      print(err);
      throw Exception('The service could not be stopped');
    }
    return true;
  }

  /// Indicates whether or not the user has given the necessary permissions in order to run in the background.
  ///
  /// Returns true, if the user has granted the permission, otherwise false.
  /// May throw a [PlatformException].
  Future<bool> get hasPermissions async {
    return await rsPlatform.invokeMethod<bool>('hasPermissions') == true;
  }

  static int _androidNotificationImportanceToInt(
      AndroidNotificationImportance importance) {
    switch (importance) {
      case AndroidNotificationImportance.Low:
        return -1;
      case AndroidNotificationImportance.Min:
        return -2;
      case AndroidNotificationImportance.High:
        return 1;
      case AndroidNotificationImportance.Max:
        return 2;
      case AndroidNotificationImportance.Default:
      default:
        return 0;
    }
  }
}

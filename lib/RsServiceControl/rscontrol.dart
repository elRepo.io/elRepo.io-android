/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/// In this file are defined the funcions related to the interaction between
/// retroshare-service, the flutter app and the operating system.
/// This functions can't be defined on the repo-lib because they have flutter
/// dependencies

import 'package:connectivity/connectivity.dart';
import 'package:elRepoIo/RsServiceControl/rscontrol_android.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'package:flutter/services.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:flutter/services.dart' as svc;
import 'package:flutter/foundation.dart' show defaultTargetPlatform, TargetPlatform;


/// Initialize Retroshare and start retroshare loop
///
/// For Android platform it also initialize downloads folder
/// Util function to initialize RS, used on Splash screen and tests
Future<void> initializeRetroShare(Function startRetroShareErrorCallback) async {
  // Callbacks to start/stop retroshare from elrepo-lib
  RsServiceControlPlatform.instance.setControlCallbacks();
  // Initialize RsServiceControl with default values
  await RsServiceControlPlatform.instance.initialize(startRetroShareErrorCallback: startRetroShareErrorCallback).then((value) async {
    print('RsServiceControl initialize Ok');
    await RsServiceControlPlatform.instance.startRetroshareLoop().then((isRunning) async {
      if (!isRunning) {
        // Show Retroshare not runing error dialog
        startRetroShareErrorCallback();
      }
    });
  });
}

//https://medium.com/flutter/how-to-write-a-flutter-web-plugin-part-2-afdddb69ece6

/// Fast implementation of a flutter package to control retroshare.
///
/// We have to move it to an independent package depending of the platform
class RsServiceControlPlatform extends PlatformInterface {

  // Implementation of the plugin PlatformInterface
  static RsServiceControlPlatform _instance;
  static RsServiceControlPlatform get instance => _instance ??= _generateInstance();
  static RsServiceControlPlatform _generateInstance() =>
    defaultTargetPlatform == TargetPlatform.android
        ?  RsServiceControlAndroid() : RsServiceControlPlatform();

  // RsServiceControlPlatform() : super(token: _token);
  // static final Object _token = Object();
  // static RsServiceControlPlatform _instance = RsServiceControlAndroid();
  // /// The default instance of [RsServiceControlPlatform] to use.
  // ///
  // /// Defaults to [MethodChannelRsServiceControl].
  // static RsServiceControlPlatform get instance => _instance;
  // /// Platform-specific plugins should set this with their own platform-specific
  // /// class that extends [RsServiceControlPlatform] when they register themselves.
  // static set instance(RsServiceControlPlatform instance) {
  //   PlatformInterface.verifyToken(instance, _token);
  //   _instance = instance;
  // }

  // Implementation of the plugin

  /// Show if [initialize] was called successfully
  var isInitialized = false;

  /// Is true when RS is running on background
  var isBackgroundExecutionEnabled = false;

  /// Function called when [startRetroShareExecution] fails
  ///
  /// Typically call something on the ui to show an error message
  Function startRsErrorCallback;

  /// Initializes the plugin.
  /// May request the necessary permissions from the user or the OS in order to
  /// run in RetroShare.
  ///
  /// Does nothing and returns true if everything Ok or already initialized,
  /// otherwise false.
  /// May throw a [PlatformException].
  Future<bool> initialize({Function startRetroShareErrorCallback}){
    startRsErrorCallback = startRetroShareErrorCallback;
    // throw UnimplementedError('initialize() has not been implemented.');
    isInitialized = true;
    return Future.value(true);
  }

  /// Enables the execution of the RS service.
  /// You must call [initialize] before calling this function.
  ///
  /// Returns true if successful, otherwise false.
  /// Throws an [Exception] if the plugin is not initialized by calling
  /// [initialize] first. May throw a [PlatformException].
  Future<bool> startRetroShareExecution() async {
    // throw UnimplementedError('runRetroShareExecution() has not been implemented.');
    if(isInitialized && await rs.isRetroshareRunning()) return isBackgroundExecutionEnabled = true;
    if(startRsErrorCallback != null) startRsErrorCallback();
    return false;
  }

  /// Stop RetroShare execution.
  /// You must to call [initialize] before calling this function.
  ///
  /// Returns true if successful, otherwise false.
  /// Throws an [Exception] if the plugin is not initialized by calling
  /// [initialize] first. May throw a [PlatformException].
  Future<bool> stopRetroShareExecution() async {
    // throw UnimplementedError('disableRetroShareExecution() has not been implemented.');
    if(isInitialized) {
      try {
        rs.RsControl.rsGlobalShutDown();
        if (!(await rs.isRetroshareRunning())) return true;
      } catch (e) {
        print("Can't stop retroshare");
        print(e);
      }
      return false;
    } else {
      throw Exception(
          'RsServiceControl plugin must be initialized before calling startRetroshareLoop()');
    }
  }

  /// This will set the callback that the [retroshare_dart_wrapper] is going to
  /// use when RS service is down.
  void setControlCallbacks() {
    rs.setStartCallback(startRetroshareLoop);
  }

  /// Used to prevent restart RS after stop it manually
  ///
  /// This only disable callback on RS library. So, technically, you could redo
  /// restart RS callback doing [startRetroshareLoop].
  void disableControlCallbacks() {
    rs.setStartCallback(null);
  }

  /// Return [false] if [rs.rsStartCallback] is null, which mean is not set
  bool isRsControllCallbackSet() => rs.rsStartCallback != null;

  /// Start Retroshare Service with a countdown of 20 tries by default
  ///
  /// If you did [disableControlCallbacks] before, it execute
  /// [setControlCallbacks] again.
  Future<bool> startRetroshareLoop([int countdown = 20]) async {
    if (isInitialized) {
      if(rs.rsStartCallback == null) setControlCallbacks();
      for (var attempts = countdown; attempts >= 0; attempts--) {
        print('Starting Retroshare Service. Attempts countdown $attempts');
        try {
          var isUp = await rs.isRetroshareRunning();
          if (isUp) {
            return isBackgroundExecutionEnabled = true;
          }
          await startRetroShareExecution();

          await Future.delayed(Duration(seconds: 2));
        } catch (err) {
          await Future.delayed(Duration(seconds: 2));
        }
      }
      return false;
    } else {
      throw Exception(
          'RsServiceControl plugin must be initialized before calling startRetroshareLoop()');
    }
  }
}


/// Used as callback for a  [_connectivity.onConnectivityChanged.listen]
/// listener.
Future<void> updateConnectionStatus(ConnectivityResult result) async {
  if (result == ConnectivityResult.wifi) {
    repo.onlineMode();
  } else {
    repo.offlineMode();
  }
}

/// Initialize RetroShare connectivity state depending on phone network used
Future<void> initConnectivity() async {
  final _connectivity = Connectivity();
  var result = ConnectivityResult.none;
  try {
    result = await _connectivity.checkConnectivity();
  } on svc.PlatformException catch (e) {
    print(e.toString());
  }
  return updateConnectionStatus(result);
}



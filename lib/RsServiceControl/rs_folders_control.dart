/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io' as Io;
import 'dart:io';
import 'package:archive/archive_io.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rscontrol;

// todo(kon): move as much as possible of this code to elrepo-lib

/// Class with related functions to manage folders like downloads, partials, or
/// [getRetrosharePath] to do the backup
class RsFoldersControl {

  /// Used to store directories
  static String downloadDir = '',  partialsDir = '';

  /// Check if we already have Android permisions
  static bool _alreadyPermitted = false;

  /// Used to get `.retroshare` path to do the backups of retroshare
  static Future<String> getRetrosharePath() async =>
      await rs.RsAccounts.configDirectory();

  /// Android only. Check to Android API if the storage permission is granted
  static Future<bool> isStoragePermissionGranted () async =>
      await Permission.storage.status.isGranted;

  /// Android only
  ///
  /// It check if the storage permissions are granted, if not, asks the user
  /// for it
  ///
  /// If the User grants it, do [prepareSystemDirectories] to configure
  /// donwload dir on retroshare.
  static Future<bool> requestFilePermissions() async {
    // If is not already granted and is not saved yet that is granted.
    // Used on the first execution
    if(!_alreadyPermitted && !await isStoragePermissionGranted()) {
      // Request permisions
      // todo(kon): add isLoggedIn check
      if (await Permission.storage.request().isGranted) {
        // Once granted, prepare the retroshare directories to download files
        prepareSystemDirectories();
        _alreadyPermitted = true;
      } else {
        throw Exception('No permission to create the download directory.');
      }
    } else {
      // If it was granted before but is the first time requested in this
      // execution.
      _alreadyPermitted = true;
    }
    return _alreadyPermitted;
  }

  /// Create `Download` and `Partials` folder on
  static Future<bool> prepareSystemDirectories() async {
    RsFoldersControl.downloadDir = await getOrCreateExternalDir('/Download');
    RsFoldersControl.partialsDir = await getOrCreateExternalDir('/Partials');
    return await repo.setDownloadAndPartialDirectory(downloadDir, partialsDir);
  }

  /// Find the absolute path where program store the data using [_getExternalPath]
  /// and create a directory if [appendToPath] is defined and not already created
  ///
  /// ANDROID only for the [Permission] ussage?
  static Future<String> getOrCreateExternalDir([String appendToPath = '']) async {
    print('Getting or creating download folder.');
    final downloadPath = await _getExternalPath() + appendToPath;
    await _createDir(downloadPath);
    return downloadPath;
  }

  /// Create a directory on the specific [path].
  ///
  /// May print an error and return false
  static Future<bool> _createDir(String path) async {
    try {
      var dir = Io.Directory(path);
      var dirExists = await dir.exists();
      if (!dirExists) {
        print('Creating directory: $path');
        await dir.create(recursive: true);
      }
    } catch (e) {
      print('Error creating directory. $e');
      return false;
    }
    return true;
  }

  /// Return paths to directories where application specific data can be stored.
  ///
  /// Briefly it return where to store downloaded data on SD card on android
  static Future<String> _getExternalPath() async {
    List extDirectories = await getExternalStorageDirectories();
    Io.Directory externalPath = extDirectories.last;
    print('Getting external storage directory: $externalPath');
    for (var extDir in extDirectories) {
      if (!extDir.path.contains('emulated')) {
        externalPath = extDir;
      }
    }
    print('Returning external storage directory: $externalPath');
    return externalPath.path;
  }
}

class RsBackup {

  /// Create a zip backup from `.retroshare` folder. Return zip path.
  ///
  /// todo(kon): implement also add shared dirs
  static Future<String> createZipBackup(List<dynamic> sharedDirs) async{
    final rsPath = await RsFoldersControl.getRetrosharePath();
    final rsDir = Directory(rsPath);

    // appDocDir seem to be the destination zip file?
    final appDocDir = await getApplicationDocumentsDirectory();
    final backupZipPath = path.join(
        appDocDir.path, 'elRepo_backup.zip');

    // Encode the Zip file
    try {
      var encoder = ZipFileEncoder();
      encoder.zipDirectory(rsDir, filename: backupZipPath);
      print('Backup ziped $backupZipPath');
      return backupZipPath;
    } catch (e) {
      print('Backup Zip failed');
      rethrow;
    }
  }

  /// Restore a zipped backup stopping and starting RS
  ///
  /// [cb] function will be called after ending [repo.getAccountStatus()]
  static Future<void> restoreBackup (String file, Function(String) cb) async {
    print('Starting restoreBackup');
    final rsDir = Directory(await RsFoldersControl.getRetrosharePath());
    print('Got backup destination path $rsDir');
    await rscontrol.RsServiceControlPlatform.instance.stopRetroShareExecution();
    print('Retroshare stopped');
    await restoreZipBackup(file, rsDir);
    await rscontrol.RsServiceControlPlatform.instance.startRetroshareLoop();
    final accountStatus = await repo.getAccountStatus();
    print('Account status $accountStatus');
    cb(accountStatus);
  }

  /// Restore a zip backup into .retroshare directory
  static Future<void> restoreZipBackup(String backupFilePath, Directory destiny) async{
    final zipFile = File(backupFilePath);

    print('Selected backup file path: $backupFilePath');
    print('Retroshare path is: ${destiny.path} with directories:');
    destiny.list(recursive: false).forEach((f) {
      print('${f.path}');
    });

    destiny.deleteSync(recursive: true);
    destiny.createSync();
    print('Original RS directory deleted');

    // Read the Zip file from disk.
    final bytes = zipFile.readAsBytesSync();
    // Decode the Zip file
    final archive = ZipDecoder().decodeBytes(bytes);
    // Extract the contents of the Zip archive to disk.
    for (final file in archive) {
      final filename = destiny.path + '/' + file.name;
      print('Restoring $filename');
      if (file.isFile) {
        final data = file.content as List<int>;
        File(filename)
          ..createSync(recursive: true)
          ..writeAsBytesSync(data);
      } else {
        Directory(filename)
            .create(recursive: true);
      }
    }
  }

}

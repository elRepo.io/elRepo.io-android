/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/avatar_or_icon.dart';
import 'package:elRepoIo/ui/common/loading_dialog.dart';
import 'package:elRepoIo/ui/common/parse_text_links.dart';
import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:elRepoIo/ui/providers/post_comments.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/models.dart' as models;

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

// todo: Maybe this have to go to elrepo-lib/repo.dart??
class _CommentData {
  models.PostMetadata metadata = models.PostMetadata(
      markup: models.MarkupTypes.plain,
      contentType: models.ContentTypes.text,
      role: models.PostRoles.comment);
  models.PostBody mBody = models.PostBody();
}

class CommentForm extends ConsumerStatefulWidget {
  final String forumId, postId;
  final AvatarOrIcon avatar;
  /// Used to update a comments list
  final UpdatableFutureBuilderController commentsController;

  CommentForm(this.forumId, this.postId,  {Key key, this.avatar, this.commentsController}) : super(key: key);

  @override
  _CommentFormState createState() => _CommentFormState();
}

class _CommentFormState extends ConsumerState<CommentForm> {
  final _formKey = GlobalKey<FormState>();
  _CommentData _data = _CommentData();

  Future<void> _send() async {
    var form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      var pr = ProgressDialog(context,
          initialMessage: S.of(context).postingComment)
        ..show();

      var msgBody = jsonEncode(_data.mBody);
      _data.metadata.summary =
          repo.summaryFromBody(_data.mBody.text);

      final forumId = widget.forumId;
      final msgId = await repo.createComment(
          forumId,
          jsonEncode(_data.metadata),
          msgBody,
          widget.postId);

      form.reset();
      _data = _CommentData();
      pr.hide(); //pop dialog
      widget.commentsController?.runFuture();
      ref.watch(
          postCommentsProvider(PostCommentsProviderParams(forumId, widget.postId)).notifier
      ).update();
      print('post Commented with id: $msgId');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Wrap(
        children: <Widget>[
          TextFormField(
            keyboardType: TextInputType.multiline,
            maxLines: 7,
            minLines: 1,
            decoration: InputDecoration(
              labelText: S.of(context).comment,
              icon: widget.avatar,
              suffixIcon: IconButton(
                icon: Icon(
                  Icons.send,
                  color: LOCAL_COLOR,
                ),
                onPressed: _send
              ),
            ),
            validator: (value) {
              if (value.isEmpty) {
                return "Can't be empty "; //todo(intl)
              }
              _data.mBody.text = value;
              return null;
            },
            // onSaved: (val) => password = val,
          ),
        ],
      ),
    );
  }
}

Widget commentCardLoader(Map<String, dynamic> commentData) {
  final int publishTs = commentData['mMeta']['mPublishTs']['xint64'];
  final Map<String, dynamic> commentMsg = jsonDecode(commentData['mMsg']);
  final Map<String, dynamic> commentMeta = commentData['mMeta'];
  final String authorId = commentMeta['mAuthorId'];
  final idDetails = repo.getIdDetails(authorId);
  final defaultName = '...';

  /// If [idData] is null, will show the skeleton of the card anyway
  Widget _card (context, [Identity idData]) {
    return ListTile(
        leading: Container(
          padding: EdgeInsets.only(right: 5.0),
          decoration: BoxDecoration(),
          child: AvatarOrIcon(idData?.avatar ?? '',
            altText: idData?.name ?? defaultName, cacheKey: idData?.mId ?? '',),
        ),
        title: InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/identityDetails',
                arguments: IdentityArguments(authorId));
          },
          child: Text(idData?.name ?? defaultName,
              style: TextStyle(color: LOCAL_COLOR)),
        ),
        subtitle: Column(
          children: [
            Align(
                alignment: Alignment.topLeft,
                child: ElRepoioParsedText(commentMsg['text'])
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: TimeText(
                publishTs,
                showIcon: false,

              )
            )

          ],
        ));
  }

  return Card(
    elevation: 2.0,
    margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
    child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        //decoration: BoxDecoration(color: Colors.white),
        child: FutureBuilder(
            future: idDetails,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                print('Error: ${snapshot.error}');
                return Center(
                  child: Text(S.of(context).cannotLoadContent),
                );
              } else if (snapshot.hasData == false ||
                  snapshot.data == null) {
                return _card(context);
              } else {
                return _card(context, snapshot.data);
              }
            })
    ),
  );
}

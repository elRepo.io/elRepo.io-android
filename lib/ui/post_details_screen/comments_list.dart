/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/avatar_or_icon.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/ui/post_details_screen/comments_components.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:elRepoIo/ui/providers/post_comments.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CommentsView extends ConsumerStatefulWidget {
  final String forumId, postId;
  final PostArguments postArgs;
  const CommentsView(this.forumId, this.postId, this.postArgs,
      {Key key}) : super(key: key);

  @override
  _CommentsViewState createState() => _CommentsViewState();
}

class _CommentsViewState extends ConsumerState<CommentsView> {

  @override
  void initState() {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => update());
    super.initState();
  }

  void update() {
    if (mounted) {
      ref.watch(postCommentsProvider(
          PostCommentsProviderParams(widget.forumId, widget.postId))
          .notifier)
          .update();
    }
  }

  @override
  Widget build(BuildContext context) {
    return
      UpdatableListPage<Map<String, dynamic>>(
        postCommentsProvider(PostCommentsProviderParams(widget.forumId, widget.postId),),
        callbackWidget: (data) => commentCardLoader(data),
        timerCallback: (ref, t) => update(),
        noDataWidget: (ref) => ElevatedButton.icon(
          style: ButtonStyle(
            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(0)),
            shadowColor: MaterialStateProperty.all(Colors.transparent),
            backgroundColor: MaterialStateProperty.all(Colors.transparent),
          ),
          label: Text('No comments yet!', style: TextStyle(color: NEUTRAL_COLOR), ),// todo(intl)
          icon: Icon(Icons.refresh_outlined, color: NEUTRAL_COLOR,),
          onPressed: () async => update(),
        ),
      );
  }
}

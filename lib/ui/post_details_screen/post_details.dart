/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */


import 'dart:io';

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/circles/circle_card.dart';
import 'package:elRepoIo/ui/common/file_download_card.dart';
import 'package:elRepoIo/ui/common/identity_card.dart';
import 'package:elRepoIo/ui/common/image_preview.dart';
import 'package:elRepoIo/ui/common/parse_text_links.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/providers/identities_following.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:elRepoIo/user_preferences.dart' show USER_PREFERENCES;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:open_file/open_file.dart';
import 'dart:async';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:share_plus/share_plus.dart';
import 'package:elrepo_lib/models.dart' as models;
import 'package:flutter_parsed_text/flutter_parsed_text.dart';

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';


class PostDetail extends ConsumerStatefulWidget {
  final PostArguments args;

  const PostDetail(this.args, {Key key}) : super(key: key);

  @override
  _PostDetailState createState() => _PostDetailState();
}

class _PostDetailState extends ConsumerState<PostDetail> {
  // Post and forum ids
  String postId;
  String forumId;

  // Post details
  Future<Map<String, dynamic>> postDetails; // Future raw post details
  models.PostBody postBody; // Parsed post message body
  models.PostMetadata postMetadata; // Parsed post title mMeta
  bool isOwnPost = false;

  // Related to files
  IconData contentTypeIcon;
  bool hasPayload = false; // There are payload links
  bool isBookmarked; // Used to know if a post without payload is bookmarked
  String hash;
  String fileLink;
  FileDownloadCardController downloadCardController = FileDownloadCardController();

  // Author identity
  String authorId;
  String authorName = '';
  Identity idDetails; // This is retrieved from backend and then passed to identity details screen

  // Related to circles
  Map<String, dynamic> circleDetails;

  // Get this post details from /rsGxsForums/getForumContent
  Future<Map<String, dynamic>> _getPostDetails() =>
      repo.getPostDetails(widget.args.forumId, widget.args.postId, false);

  // Get identity details
  void _getIdDetails(String authorId) {
    repo.getIdDetails(authorId).then((details) {
      if (details == null){
        authorName = authorId;
      }
      else if (authorName == '') {
        if (mounted) {
          setState(() {
            authorName = details.name;
            idDetails = details;
          });
        }
      }
    }).catchError(
            (error) => print('error retrieving identity details. $error'));
  }

  // Mark a post as bookmarked
  void _bookmarkPost(context) {
    if (!isOwnPost) {
      // Show snackbar
      String snackBarText;
      snackBarText = S.of(context).bookmarkContent;
      final snackBar = SnackBar(
        content: Text(snackBarText),
        duration: Duration(seconds: 5),
        onVisible: () => {},
      );
      Scaffold.of(context).showSnackBar(snackBar);
      repo.bookmarkPost(forumId, postId, postMetadata).then(
              (value) {
            if (mounted) {
              // Update providers
              // This will prevent that list is not rebuilt so the "bookmarked" icon don't appear
              ref.watch(allBookmarksProvider.notifier).update();
              ref.watch(explorePostsProvider.notifier).lazyUpdate();
              ref.watch(identityPostsProvider(authorId).notifier).lazyUpdate();
            }
          }
      );
      postDetails = _getPostDetails();
      postDetails.then((value) {
        if (mounted) setState(() => isBookmarked = true);
      });
    }
  }

  // todo(kon) : this init state could be improved if we use little statefull widgets that control each call
  // So, instead of call setState of all the screen each time new information is got, split the screen in little parts
  // and refresh state only on this widgets, like, identity name, or is bookmarked
  @override
  void initState() {
    postDetails = _getPostDetails();
    forumId = widget.args.forumId;
    postId = widget.args.postId;

    // If post metadata provided, use it to show some of the details
    if (widget.args.postMetadata != null) {
      // Get post metadata forom post title
      postMetadata =
          models.PostMetadata.fromJsonString(widget.args.postMetadata.mMsgName);
      // Get author id details like name or avatar
      authorId = widget.args.postMetadata.mAuthorId;
      _getIdDetails(authorId);
      // Check if is the owner of the post
      isOwnPost = widget.args.postMetadata.mAuthorId == rs.authIdentityId;
    }

    // Update post detail with file information
    downloadCardController.addListener(() {
      setState(() {});
    });

    // Check if is bookmarked (to display the icons correctly)
    repo.isBookmarked(forumId, postId).then((value) {
      if (isBookmarked != value && mounted) {
        setState(() => isBookmarked = value);
      }
    });

    // Circle details
    if (circleDetails == null && postMetadata?.circle != null) {
      repo.getSubscribedCircle(postMetadata.circle).then((circle) {
        if (circleDetails != circle && mounted) {
          setState(() => circleDetails = circle);
        }
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: postDetails,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print('Error: ${snapshot.error}');
          return Center(
            child: Text(S.of(context).cannotLoadContent),
          );
        } else if (snapshot.hasData == false || snapshot.data.length == 0 ) {
          // return loadingBox();
          return _getTree();
        } else {
          final data = snapshot.data;
          return _getTree(data);
        }
      },
    );
  }

  Widget _getTree ([snapshotData]) {
    // Todo(kon): i'm not sure but I guess all this code below is not at their place

    // In case widget.args.postMetadata is not provided, get all the required information needed on post details
    // in case isn't already parsed from widget arguments on initState

    if (snapshotData != null) {
      if (widget.args.postMetadata == null) {
        // Parse post metadata from "post title"
        try {
          postMetadata = models.PostMetadata.fromJsonString(
              snapshotData['mMeta']['mMsgName']);
        } catch (error) {
          print('Error parsing metadata Json: $error');
        }

        // Author identity
        authorId = snapshotData['mMeta']['mAuthorId'];
        _getIdDetails(authorId);

        // Check if is own post
        isOwnPost = snapshotData['mMeta']['mAuthorId'] == rs.authIdentityId;
      }

      // Parse Post body
      try {
        postBody = models.PostBody.fromJson(snapshotData['mMsg']);
      } catch (error) {
        print('Error parsing body Json: $error');
        // this may be a post originated from another client
        // we are choosing to show it but we could ignore it.
        postBody.text = snapshotData['mMsg'];
        postBody.payloadLinks = [];
      }

      // Parse payload links
      if (postBody.payloadLinks.isNotEmpty) {
        hasPayload = true;
        hash = postBody.payloadLinks[0].hash;
        fileLink = postBody.payloadLinks[0].link;
      }
    }

    return Container(
        margin: defaultMargin(),
        child: Column(
          // scrollDirection: Axis.vertical,
          // shrinkWrap: true,
            children: <Widget>[
              ListTile(
                leading: Container(
                    padding: EdgeInsets.only(right: 5.0),
                    decoration: BoxDecoration(),
                    child: GestureDetector(
                      onTap: () async {
                        if(postMetadata?.hasPreview() ?? false
                            && hasPayload
                            && !downloadCardController.fileExists
                            && await showConfirmationDialog(context)) {
                          downloadCardController.requestFiles();
                        }
                      },
                      child: IconOrPreview(postMetadata, defaultIconSize: 48, iconColor: NEUTRAL_COLOR,)
                    ),
                ),
                title: ElRepoioParsedText(
                  postMetadata?.title ?? '...',
                  style: TextStyle(fontSize: 20, color: Colors.black), postMetadata: widget.args.postMetadata,
                ),
                subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 5),
                      TimeText(snapshotData == null ? DateTime.now().millisecondsSinceEpoch : snapshotData['mMeta']['mPublishTs']['xint64']),
                      SizedBox(height: 3),
                      InkWell(
                          onTap: () {
                            if(idDetails != null) {
                              Navigator.pushNamed(context, '/identityDetails',
                                  arguments:
                                  IdentityArguments(authorId,));
                            }
                          },
                          child: IdentityCardLittle(idDetails)
                      ),
                      if (circleDetails != null)
                        InkWell(
                          onTap: () async {
                            openCircleDetails(context, await rs.RsGxsCircles.getCircleDetails(postMetadata.circle));
                          },
                          child: LittleCircleCard( circleDetails['mGroupName'], fontSize: 14),
                        ),
                      Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            // Create a sized box saving the space for the icons
                            SizedBox(
                                height:
                                48.0), // Hardcoded height of the icons
                            // todo: implement post edition. Either, add icon showing that the publication is yours
                            // if (isOwnPost)
                            //    IconButton(
                            //       icon:  Icon(Icons.edit,
                            //           color: REMOTE_COLOR))
                            if (isBookmarked != null &&
                                (isBookmarked || isOwnPost))
                            // todo: add a tooltip showing something like: you have this content locally
                              IconButton(
                                onPressed: () {},
                                icon: Icon(ICON_BOOKMARKED_TRUE, color: LOCAL_COLOR),
                              ),
                            // Give the possibility to bookmark content without payload
                            if (isBookmarked != null &&
                                !isBookmarked &&
                                !isOwnPost)
                              IconButton(
                                icon: Icon(ICON_BOOKMARKED_FALSE,
                                    color: LOCAL_COLOR),
                                onPressed: () => _bookmarkPost(context),
                              ),
                            // Share icon.
                              IconButton(
                                  icon:
                                  Icon(Icons.share, color: REMOTE_COLOR),
                                  onPressed: _shareAction ),
                          ])
                    ]),
              ),
              if (hasPayload)
                FileDownloadCard(
                  fileHash: hash,
                  fileLink: fileLink,
                  controller: downloadCardController,
                  callback: () {
                    _bookmarkPost(context);
                  },
                ),
              Container(
                  padding: defaultMargin(),
                  decoration: BoxDecoration(
                      border: Border(
                          top:
                          BorderSide(width: 1.0, color: Colors.black12))),
                  child: Padding(
                    padding: const EdgeInsets.all(.01),
                    child: Column(children: [
                      if (hasPayload &&
                          downloadCardController.fileExists &&
                          postMetadata.contentType == 'image' &&
                          downloadCardController.fileInfo != null)
                        Column(
                          children: <Widget>[
                            ImagePreview(downloadCardController.absolutePath),
                            if(USER_PREFERENCES.ACTIVATE_PERCEPTUAL_SEARCH)
                              ElevatedButton.icon(
                                onPressed: () async {
                                  Navigator.pushNamed(context, '/search',
                                      arguments: SearchArguments(
                                          downloadCardController.fileInfo));
                                },
                                label: Text(S.of(context).searchSimilar),
                                icon: Icon(Icons.search),
                                style: ButtonStyle(
                                    backgroundColor:
                                    MaterialStateProperty.all(LOCAL_COLOR)),
                              ),
                          ],
                        ),
                      ElRepoioParsedText(postBody?.text ?? ' ',  postMetadata: widget.args.postMetadata,),
                    ]),
                  )),
            ])
    );
  }

  void _shareAction () async {
    final signature = '-----'
        '\nPublication from https://elrepo.io'
        '\nThe decentralized repository of culture';
    var shareText =
        '${postMetadata.title}\n ${postMetadata.summary}\n$signature';
    if (hasPayload && downloadCardController.fileExists) {
      final filePath =
      await repo.getPathIfExists(
          postBody.payloadLinks[0].hash);
      Share.shareFiles([filePath],
          text: shareText);
    } else {
      Share.share(shareText);
    }
  }

  Future<dynamic> showConfirmationDialog(BuildContext context) {
    var alert = AlertDialog(
      title: Text('Download file?'), // todo(intl)
      content: Text('This is a preview. Do you want to download the complete file?'), // todo(intl)
      actions: [
        TextButton(
          child: Text(S.of(context).cancel),
          onPressed:  () => Navigator.pop(context, false),
        ),
        TextButton(
          child: Text(S.of(context).ok),
          onPressed:  () => Navigator.pop(context, true),
        ),  // set up the AlertDialog
      ],
    );  // show the dialog
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

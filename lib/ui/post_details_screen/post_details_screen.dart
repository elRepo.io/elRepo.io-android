/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */


import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/avatar_or_icon.dart';
import 'package:elRepoIo/ui/post_details_screen/comments_components.dart';
import 'package:elRepoIo/ui/post_details_screen/comments_list.dart';
import 'package:elRepoIo/ui/post_details_screen/post_details.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:elRepoIo/constants.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Page that show, first the post details, and then comments.
class PublicationPage extends ConsumerStatefulWidget {
  final PostArguments args;

  PublicationPage(this.args);

  @override
  _PublicationPageState createState() => _PublicationPageState();
}

class _PublicationPageState extends ConsumerState<PublicationPage> {

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ownId = ref.watch(ownIdentityProvider);
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          'elRepo.io',
          style: TextStyle(
            color: WHITE_COLOR,
          ),
        ),
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
      ),
      body: Stack(
        children: [
          ListView(
            children: <Widget>[
              PostDetail(widget.args),
              Container(
                padding: defaultMargin(),
                decoration: BoxDecoration(
                    border: Border(
                        top:
                        BorderSide(width: 1.0, color: Colors.black12))),
              ),
              CommentsView(widget.args.forumId, widget.args.postId, widget.args),
              SizedBox(height: 200,)
            ],),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                   top: BorderSide(width: 1.0, color: Colors.black12)),

              ),
              margin: EdgeInsets.only(
                  top: 6.0, ),
              padding: EdgeInsets.only(
                  left: 17.0, right: 17.0, top: 6.0, bottom: 15.0),
              child: CommentForm(
                widget.args.forumId, widget.args.postId,
                avatar: AvatarOrIcon( ownId.avatar,
                  altText: ownId.name, cacheKey: ownId.mId,
                ),
              ),
            ),
          ),
        ],
      ),

    );
  }
}

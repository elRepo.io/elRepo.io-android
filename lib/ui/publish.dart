/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart';
import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/image_preview.dart';
import 'package:elRepoIo/ui/common/language_codes.dart';
import 'package:elRepoIo/ui/common/loading_dialog.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/providers/circles_provider.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:elrepo_lib/models.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:flutter/services.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart' as fileDialog;
import 'package:elrepo_lib/models.dart' as models;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:open_file/open_file.dart';
import 'package:path/path.dart' as path;

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';

import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:file_picker/file_picker.dart';

import 'package:mime/mime.dart';

class PublishScreen extends StatelessWidget {
  final PublishFormArgs args;
  PublishScreen ({this.args});
  final GlobalKey<_PublishFormState> _formKey = GlobalKey<_PublishFormState>(); // Used to call publish function from floating action button

  static Route<dynamic> route() => MaterialPageRoute(
    builder: (context) => PublishScreen(),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: WHITE_COLOR,
      // Button used to publish content
      floatingActionButton: FloatingActionButton(
          backgroundColor: REMOTE_COLOR,
          onPressed: () async {
            await _formKey.currentState.publish();
          },
          child: const Icon(Icons.send)),
      body: Container(
        margin: defaultMargin(),
        child: ListView(children: <Widget>[PublishForm(args: args, key: _formKey,)]),
      ),
    );
  }
}

class PublishForm extends ConsumerStatefulWidget {
  final PublishFormArgs args;
  PublishForm({Key key, this.args}) : super(key: key);

  @override
  _PublishFormState createState() => _PublishFormState();
}

class _PublishFormState extends ConsumerState<PublishForm>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  String selectedCircle;
  var selectedLangsIndex = <int>[];
  var selectedLangsCodes = <String>[];
  models.PostData postData = models.PostData();
  var _teDescription = TextEditingController();
  // Show advanced menu
  var _showAdvanced = false;
  // Used to animate advanced search icon
  AnimationController _animationController;
  bool isPlaying = false;

  @override
  void initState() {
    if(widget.args != null) {
      _teDescription.text = widget.args.descriptionText;
      if(widget.args.selectedFilePath != null &&
          widget.args.selectedFilePath.isNotEmpty) {
        _selectAbsoluteFilePath(widget.args.selectedFilePath);
      }
    }
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      ref.watch(circlesProvider.notifier).update();
    });
    super.initState();
  }

  /// For a given [filePath] get the absolute path and select it on form for
  /// publish it
  void _selectAbsoluteFilePath(String filePath) async {
    if(defaultTargetPlatform == TargetPlatform.android) {
      await RsFoldersControl.requestFilePermissions();
      try {
        postData?.filePath =
        await FlutterAbsolutePath.getAbsolutePath(filePath);
      } on PlatformException catch (e) {
        if (e.code == PlatformErrorCodes.IS_REMOTE_FILE) {
          _showInfoDialog(context, 'This file is in a cloud system', // todo(intl)
              'Please, download the file first before share it on elrepo.io network'); // todo(intl)
        } else if (e.code == PlatformErrorCodes.PATH_IS_NULL) {
          _showInfoDialog(context, 'Unexpected error',  // todo(intl)
              "Can't access to file, please try another"); // todo(intl)
        }
        // Rethrow it anyway  to track it via Sentry
        rethrow;
      }
    } else {
      postData?.filePath = filePath;
    }

    print('Selected file: $filePath ');
    print('Absolute Path: ${postData?.filePath}');

    if (postData?.filePath == null) {
      postData.filename = '';
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Error retrieving file path') //todo(intl)
      ));
      throw ('Error retrieving absolute path for $filePath with resulting path ${postData?.filePath}');
    } else {
      postData.filename = path.basename(postData.filePath);
    }
    if(mounted) setState(() { });
  }

  /// Check if selected file is an image
  bool _isAnImage() => postData?.filename != null && postData?.filename?.isNotEmpty
      && lookupMimeType(postData?.filePath).startsWith('image/');

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(labelText: S.of(context).title),
              validator: (value) {
                if (value.isEmpty) return S.of(context).titleCannotBeEmpty;
                postData.metadata.title = value;
                return null;
              },
            ),
            TextFormField(
              controller: _teDescription,
              maxLines: 9,
              decoration: InputDecoration(
                  alignLabelWithHint: true,
                  labelText: S.of(context).description),
              validator: (value) {
                postData.mBody.text = value;
                return null;
              },
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              S.of(context).youCanAddHashTags,
              style: TextStyle(
                color: REMOTE_COLOR,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Divider(
              height: 2,
              color: Colors.grey,
            ),
            RaisedButton.icon(
                onPressed: () async {
                  String filePath;
                  if(defaultTargetPlatform == TargetPlatform.android) {
                    final params = fileDialog.OpenFileDialogParams(
                        localOnly: true, copyFileToCacheDir: false);
                    filePath = await fileDialog.FlutterFileDialog.pickFile(
                        params: params);
                  } else {
                    filePath = (await FilePicker.platform.pickFiles()).files.single.path;
                  }
                  if (filePath == null) return;
                  _selectAbsoluteFilePath(filePath);
                },
                label: Text(S.of(context).chooseFile),
                icon: Icon(Icons.attach_file),
                color: ACCENT_COLOR,
                textColor: WHITE_COLOR),
            if(_isAnImage())
              SizedBox(
                height: 16,
              ),
            if (_isAnImage())
              ImagePreview(postData?.filePath),
            // Show selected file name
            if (postData?.filename?.isNotEmpty ?? false)
              Row(children: [
                IconButton(
                    color: NEUTRAL_COLOR,
                    icon: Icon(Icons.cancel_outlined),
                    onPressed: () {
                      postData?.filename = null;
                      postData?.filePath = null;
                      setState(() {});
                    }),
                Expanded(
                    child: GestureDetector(
                      onTap: () async{
                        final openStatus = await OpenFile.open(postData.filePath);
                        print('Open file result: ${openStatus.message}');
                      },
                      child: Text(
                        postData?.filename,
                        style: TextStyle(color: NEUTRAL_COLOR),
                      ),
                    )),
              ]),
            RaisedButton.icon(
                onPressed: () async {
                  setState(() {
                    _showAdvanced = !_showAdvanced;
                    isPlaying = !isPlaying;
                    isPlaying
                        ? _animationController.forward()
                        : _animationController.reverse();
                  });
                },
                label: Text('Options'), // todo(intl)
                // icon: Icon(Icons.settings),
                icon: AnimatedIcon(
                  icon: AnimatedIcons.menu_close,
                  progress: _animationController,
                  color: WHITE_COLOR,
                ),
                color: ACCENT_COLOR,
                textColor: WHITE_COLOR),
            if(_showAdvanced)_advancedOptionsMenu(),
          ],
        ),
      ),
    );
  }

  Future<void> publish() async {
    var currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    final form = _formKey.currentState;
    if (form.validate() && await _showConfirmationDialog(context)) {
      form.save();
      print(
          'Post form received file path: ${postData.filePath}');
      var pr = ProgressDialog(context,
          initialMessage: S.of(context).publishingContent)
        ..show();

      postData.metadata.circle = selectedCircle;

      postData.mBody.tags =
          getLanguagesNamesFromCodeList(selectedLangsCodes);

      repo.publishPost(postData,
          cb: (PublishEvents event, String status) async {
            switch (event) {
              case PublishEvents.BOOKMARKING:
                pr.update(
                    message:
                    'Finishing the publication'); // todo(intl)
                break;
              case PublishEvents.PUBLISHING_CONTENT:
                pr.update(
                    message: 'Publishing content...'); // todo(intl)
                break;
              case PublishEvents.HASHING_FILE:
                pr.update(
                    message: 'Hashing file $status'
                        '\nIf you have added heavy files this may take several minutes to complete.'); // todo(intl)
                break;
              case PublishEvents.CREATING_TAGS:
                pr.update(
                    message:
                    'Linking #$status found in your publication'); // todo(intl)
                break;
            }
          }).then((publishData) {
        selectedCircle = null;
        selectedLangsIndex = <int>[];
        _teDescription = TextEditingController();
        form.reset();
        postData = models.PostData();
        ref.watch(userPostsProvider.notifier).update(); // Update published teasers
        pr.hide(); //pop dialog
        setState(() {});
        final snackBar = SnackBar(
          content: Text(S.of(context).contentPublished),
          duration: Duration(seconds: 6),
          action: SnackBarAction(
            label: S.of(context).checkItOut,
            onPressed: () {
              Navigator.pushNamed(context, '/postdetails',
                  arguments: PostArguments(
                    publishData.forumId,
                    publishData.msgId,
                  ));
            },
          ),
        );
        Scaffold.of(context).showSnackBar(snackBar);
        print(
            'post Published on forum ${publishData.forumId}\n with id: ${publishData.msgId}\n and tags: ${publishData.hashTags}');
      });
    }
  }

  Widget _advancedOptionsMenu (){
    var circles = ref.watch(belongedCirclesProvider);
    return AnimatedContainer(
      duration: Duration(seconds:1),
      margin: EdgeInsets.only(top: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(
            height: 2,
            color: Colors.grey,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            'Select publications language to make it easier to index your '
                'content:',
            style: TextStyle(
              color: REMOTE_COLOR,
            ),
          ),
          languagesDropDown(selectedLangsIndex, onChanged: (indexs, codes) {
            setState(() {
              selectedLangsIndex = indexs;
              selectedLangsCodes = codes;
            });
          }),
          SizedBox(
            height: 8,
          ),
          if(circles.isNotEmpty)
            Text(
              'Restrict your publication visibility to a circle of friends',
              style: TextStyle(
                color: REMOTE_COLOR,
              ),
            ),
          Visibility(
              visible: circles.isNotEmpty,
              child: Stack(children: <Widget>[
                Container(
                    padding: EdgeInsets.only(left: 44.0),
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: selectedCircle,
                      hint: Text(S.of(context).circleSelectCircle),
                      icon: const Icon(
                        Icons.arrow_downward,
                        color: LOCAL_COLOR,
                      ),
                      iconSize: 24,
                      elevation: 16,
                      // style: const TextStyle(color: Colors.deepPurple),
                      underline: Container(
                        height: 2,
                        color: LOCAL_COLOR,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          newValue.isNotEmpty
                              ? selectedCircle = newValue
                              : selectedCircle = null;
                        });
                      },
                      items: circles
                          .map<DropdownMenuItem<String>>((dynamic circle) {
                        return DropdownMenuItem<String>(
                            value: circle['mGroupId'],
                            child: Row(
                              children: [
                                Icon(Icons.vpn_key, color: LOCAL_COLOR),
                                Text(' ' + circle['mGroupName']),
                              ],
                            ));
                      }).toList()
                      // Add "default" value
                        ..add(DropdownMenuItem<String>(
                          value: '',
                          child: Row(children: [
                            Icon(Icons.cancel, color: LOCAL_COLOR),
                            Text(' ' + S.of(context).cancel),
                          ]),
                        )),
                    )),
                Container(
                  margin: EdgeInsets.only(
                    top: 15.0,
                  ),
                  child: Icon(
                    Icons.animation,
                    color: LOCAL_COLOR,
                  ),
                ),
              ])),
        ],
      ),
    );
  }

  /// Show confirm publication dialog
  ///
  /// Should return true or false depending on user response
  Future<bool> _showConfirmationDialog(BuildContext context, ) async {
    var _return = false;
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Confirm publication'), // todo(intl)
          content: Text('Publication content won\'t be editable later!'), // todo(intl)
          actions: [
            TextButton(
              onPressed: () {
                _return = false;
                Navigator.of(context).pop();
              },
              child: Text('Cancel'), // todo(intl)
            ),
            TextButton(
              onPressed: () {
                _return = true;
                Navigator.of(context).pop();
              },
              child: Text('Ok'), // todo(intl)
            ),
          ],
        );
      },
    );
    return _return;
  }

  void _showInfoDialog(BuildContext context, String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title), // todo(intl)
          content: Text(content), // todo(intl)
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Ok'), // todo(intl)
            ),
          ],
        );
      },
    );
  }
}


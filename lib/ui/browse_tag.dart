/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/posts_teaser.dart';
import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/ui/providers/browse_tag_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:elrepo_lib/repo.dart' as repo;

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

import 'common/buttons.dart';

class BrowseTagScreen extends ConsumerStatefulWidget {
  final TagArguments args;
  BrowseTagScreen(this.args);

  @override
  _BrowseTagScreenState createState() => _BrowseTagScreenState();
}

class _BrowseTagScreenState extends ConsumerState<BrowseTagScreen> {
  @override
  void initState() {
    repo.syncForums();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      ref.read(browseTagProvider(widget.args.tagName).notifier).update();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var tagProvider = browseTagProvider(widget.args.tagName);
    var posts = ref.watch(tagProvider);
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(S.of(context).elRepoio,
            style: TextStyle(
              color: WHITE_COLOR,
            )),
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            title: Text('#${widget.args.tagName}',
              style: TextStyle(
                fontSize: 24,
                color: ACCENT_COLOR,
                fontWeight: FontWeight.w500,
              ),
            ),
            subtitle: Text('Showing posts with hashtag...'), // todo(intl)
          ),
          if(widget.args.postMetadata != null && posts.isEmpty) PostTeaserCard(widget.args.postMetadata),
          UpdatableListPage<RsMsgMetaData>(
            tagProvider,
            timerCallback: (ref, timer){
              ref.watch(tagProvider.notifier).update();
            },
            noDataWidget: (ref) => SynchronizingPostsButton(
                S.of(context).commonButtonsSynchronizePosts,
                onPressed: () => ref.watch(tagProvider.notifier).update() ),
            callbackWidget: (data) =>  PostTeaserCard(data,),
          ),
        ],
      ),
    );
  }
}

/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */


import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:elrepo_lib/repo.dart' as repo;

/// This provider is used to cache avatars loaded, listening if they have a
/// change, and updating it if necessary, without make the image blinking on
/// each setState.
///
/// See https://api.flutter.dev/flutter/widgets/Image/gaplessPlayback.html for
/// more information.
final imagesMapProvider  = StateNotifierProvider<ImagesMapNotifier, Map<String, ImageAndBase64>>((ref) {
  return ImagesMapNotifier();
});

class ImagesMapNotifier extends StateNotifier<Map<String, ImageAndBase64>> {
  ImagesMapNotifier(): super(<String, ImageAndBase64>{});

  /// Update cache entry from a already processed [MemoryImage]
  ///
  /// Return the updated [_ImageAndBase64]
  ImageAndBase64 updateImage(String key, String b64) {
    state[key] = ImageAndBase64(b64,);
    state = {...state};
    return state[key];
  }

  /// Return true if doesn't exist on the dictionary or the b64 strings are
  /// different.
  ///
  /// Will return false if [b64] is empty
  bool _isCachedImageOutdated(String key, String b64) =>
      !state.containsKey(key) || (b64.isNotEmpty && state[key].base64String != b64);

  void updateIfOutdated(String key, String b64) {
    if(_isCachedImageOutdated(key, b64)){
      if(b64.isEmpty) {
        repo.getIdDetails(key).then((details) {
          updateImage(key, details.avatar);
        });
      }
      else {
        updateImage(key, b64);
      }
    }
  }
}

/// Inner class used to store the memory image together with the corresponding
/// b64string, needed for comparisons and updates
class ImageAndBase64 {
  MemoryImage memoryImage;
  String base64String;
  ImageAndBase64(this.base64String,){
    memoryImage = ImageAndBase64.generateMemoryImage(base64String);
  }

  /// Generate a [MemoryImage] from a [base64string];
  static MemoryImage generateMemoryImage(String base64string) =>
      base64string.isNotEmpty ? MemoryImage(base64.decode(base64string)) : null;
}



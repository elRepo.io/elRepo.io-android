/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/providers/models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

final nodeProvider = StateProvider.family<Map<String, dynamic>, String>(
        (ref, locationId) =>
    ref.watch(nodesProvider).firstWhere((element) => element['id'] == locationId, orElse: () => null));

final nodesProvider  = StateNotifierProvider<NodesNotifier, List<dynamic>>((ref) {
  return NodesNotifier();
});

class NodesNotifier extends UpdatableNotifier<List<dynamic>> {
  NodesNotifier(): super([]);

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  void _updateState(value) {
    if (!listEquals(state, value)) {
      state = value;
    }
    alreadyUpdating = false;
  }

  @override
  Future<void> update() async {
    print("Updating getSslPeerList, alreadyUpdating: $alreadyUpdating");

    if(!alreadyUpdating) {
      alreadyUpdating = true;
      repo.getSslPeerList()
          .then((nodeList) {
        // If Tiers are not friends, try Tier befriending
        if ((nodeList.firstWhere((node) => repo.isTierOne(node['gpg_id']),
            orElse: () => null)) == null) {
          print("Node list empty, trying to befriendingTiers");
          repo.befriendingTiers().then((beFriendingResult) async {
            beFriendingResult
                ? _updateState(await repo.getSslPeerList())
                : alreadyUpdating = false;
          });
        } else {
          _updateState(nodeList);
        }
      }).onError((error, stackTrace) {
        alreadyUpdating = false;
      });
    }
  }

  /// Delete a node from the state
  ///
  /// Use `data['gpg_id']` from a location
  Future<void> delete(String gpgId) async {
    await rs.RsPeers.removeFriend(gpgId);
    state = [
      ...state..removeWhere((element) => element['gpg_id'] == gpgId)
    ];
  }
}
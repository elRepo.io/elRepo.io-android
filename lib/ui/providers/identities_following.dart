
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:ui';

import 'package:elRepoIo/ui/providers/models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/rs_models.dart';

/// Return a count of user published posts
final identityPostsPublishedCount  = StateProvider.family<int, String>((ref, identityId) =>
    ref.watch(identityPostsProvider(identityId)).length
);

/// Return posts for specific [identityId] calling [repo.getUserForumSortedMsgs]
final identityPostsProvider  = StateNotifierProvider
    .family<IdentityPostsNotifier, List<RsMsgMetaData>, String>
  ((ref, identityId) {
  return IdentityPostsNotifier(identityId);
});

class IdentityPostsNotifier extends UpdatableNotifier<List<RsMsgMetaData>> implements LazyUpdateNotifier<List<RsMsgMetaData>>  {
  // You need to specify forum and post id
  String identityId;

  IdentityPostsNotifier(this.identityId): super(<RsMsgMetaData>[]);

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  @override
  Future<void> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      print("Syncthing id posts $identityId");
      repo.getUserForumSortedMsgs(identityId).then(
              (value) async {
            if (!listEquals(state, value)) state = value;
            alreadyUpdating = false;
          }
      );
    }
  }

  @override
  void lazyUpdate() => state = [...state];
}


final followingCardProvider  = StateNotifierProvider<FollowingCardNotifier, List<dynamic>>((ref) {
  return FollowingCardNotifier();
});

/// Get [repo.getFollowedAuthorsInfo()]
class FollowingCardNotifier extends UpdatableNotifier<List<dynamic>> {
  FollowingCardNotifier(): super([]);

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  @override
  Future<void> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      await repo.getFollowedAuthorsInfo().then((value) {
        value.removeWhere((id) => repo.isIdOwnedByTier(id));
        if (!listEquals(state, value)) state = value;
        alreadyUpdating = false;
      });
    }
  }
}
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final themeNotifierProvider = ChangeNotifierProvider<ThemeNotifier>((ref) => ThemeNotifier());
class ThemeNotifier with ChangeNotifier {
  bool _isDarkThemeEnabled = false;

  ThemeData get currentTheme => _isDarkThemeEnabled
      ? ThemeData.dark().copyWith(
    primaryColor: REMOTE_COLOR,
    accentColor: PURPLE_COLOR,
    buttonTheme: ButtonThemeData(
      buttonColor: REMOTE_COLOR,
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: REMOTE_COLOR,
      foregroundColor: Colors.white,
    ),
    textTheme: TextTheme(
      headline3: TextStyle(
          color: WHITE_COLOR, fontWeight: FontWeight.bold, fontSize: 21),
    ),
    toggleableActiveColor: REMOTE_COLOR,
  )
      : ThemeData.light().copyWith(
    primaryColor: LOCAL_COLOR,
    accentColor: ACCENT_COLOR,
    backgroundColor: WHITE_COLOR,
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: LOCAL_COLOR),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: LOCAL_COLOR),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: ACCENT_COLOR),
      ),
    ),
    buttonTheme: ButtonThemeData(
      buttonColor: REMOTE_COLOR,
    ),

//TODO lookup why the text is white when the textTheme is active
    /* textTheme: TextTheme(
            display1:
                TextStyle(color: REMOTE_COLOR, fontWeight: FontWeight.bold),
            display2: TextStyle(
                color: BTN_FONT_COLOR,
                fontWeight: FontWeight.bold,
                fontSize: 20),
            display3: TextStyle(
                color: LOCAL_COLOR,
                fontWeight: FontWeight.bold,
                fontSize: 22),
            button:
                TextStyle(color: LOCAL_COLOR, fontSize: 17,),
            subtitle:
                TextStyle(color: ACCENT_COLOR, fontWeight: FontWeight.normal),
            headline:
                TextStyle(color: LOCAL_COLOR, fontWeight: FontWeight.normal),
          ),

    inputDecorationTheme: InputDecorationTheme(
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: NEUTRAL_COLOR.withOpacity(.4),
        ),
      ),
    ),
    */
  );

  bool get isDarkThemeEnabled => _isDarkThemeEnabled;

  void setDarkTheme(bool b) {
    _isDarkThemeEnabled = b;
    notifyListeners();
  }
}

/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/providers/models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/models.dart';

/// This file stores diferent providers that get a list of teasers from el
/// repo-lib. For example posts teasers, or identity teasers.
///
/// Is the evolution of the [UpdatableFutureBuilder] logic

final allBookmarksProvider  = StateNotifierProvider<BookmarksNotifier, List<RsMsgMetaData>>((ref) {
  return BookmarksNotifier();
});

final downloadingProvider = StateProvider<List<RsMsgMetaData>>((ref) =>
    ref.watch(allBookmarksProvider).where((element) => element.isDownloading).toList()
);

final downloadedProvider = StateProvider<List<RsMsgMetaData>>((ref) =>
    ref.watch(allBookmarksProvider).where((element) => !element.isDownloading).toList()
);

class BookmarksNotifier extends UpdatableNotifier<List<RsMsgMetaData>> {
  BookmarksNotifier(): super(<RsMsgMetaData>[]);

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  @override
  Future<void> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      await repo.getBookmarkPosts().then((value) {
        if (!listEquals(state, value)) state = value;
        alreadyUpdating = false;
      });
    }
  }
}

/// Get own user posts
final userPostsProvider  = StateNotifierProvider<UserPostsNotifier, List<RsMsgMetaData>>((ref) {
  return UserPostsNotifier();
});

class UserPostsNotifier extends UpdatableNotifier<List<RsMsgMetaData>> {
  UserPostsNotifier( ): super(<RsMsgMetaData>[]);

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  @override
  Future<void> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      await repo.getOwnPublishedPostHeaders().then((value) {
        if (!listEquals(state, value)) state = value;
        alreadyUpdating = false;
      });
    }
  }
}

final explorePostsProvider  = StateNotifierProvider<ExplorePostsNotifier, List<RsMsgMetaData>>((ref) {
  return ExplorePostsNotifier();
});

class ExplorePostsNotifier extends UpdatableNotifier<List<RsMsgMetaData>> implements LazyUpdateNotifier<List<RsMsgMetaData>>  {
  ExplorePostsNotifier( ): super(<RsMsgMetaData>[]);

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  @override
  Future<void> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      await repo.exploreContent().then((value) {
        if (!listEquals(state, value)) state = value;
        alreadyUpdating = false;
      });
    }
  }

  @override
  void lazyUpdate () => state = [...state];
}


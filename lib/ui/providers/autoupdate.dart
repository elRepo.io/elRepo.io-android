/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */


import 'package:elRepoIo/ui/providers/circles_provider.dart';
import 'package:elRepoIo/ui/providers/identities_following.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:elRepoIo/ui/providers/models.dart';
import 'package:elRepoIo/ui/providers/nodes_provider.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// File with providers auto update logic
///
/// Used to retrieve updates from RS every X seconds


/// Providers to update on first execution
final firstUpdateProviders =  [
  allBookmarksProvider,
  downloadedProvider,
  downloadingProvider,
  userPostsProvider,
  explorePostsProvider,
  followingCardProvider,
  ownIdentityProvider,
  circlesProvider,
  nodesProvider,
];

void updateProvidersList(WidgetRef ref, List providersList) {
  print('Performing update providers');
  for (var provider in providersList) {
    if(provider is StateNotifierProvider<UpdatableNotifier, dynamic>) {
      ref.watch(provider.notifier).update();
    }
    else {
      ref.watch(provider);
    }
  }
}
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:elrepo_lib/repo.dart' as repo;

class ConnectionStatus {
  String statusMessage, explanation;
  IconData icon;
  Color color;
  ConnectionStatus({this.statusMessage, this.explanation, this.icon, this.color});
}

final Map<repo.RsNetworkStatus, ConnectionStatus> statusStateMap = {
  repo.RsNetworkStatus.NO_PEERS : ConnectionStatus (
    statusMessage : 'Not connected!',
    explanation: 'No Peer connections. Data will be synchronized when you are connected.', // todo(intl)
    icon: Icons.signal_cellular_null,
    color: RED_COLOR,
  ),
  repo.RsNetworkStatus.PEER_CONNECTED : ConnectionStatus (
    statusMessage : 'Partially connected',
    explanation: 'Some peers connected. Synchronization might be slow or incomplete', // todo(intl)
    icon: Icons.signal_cellular_connected_no_internet_4_bar,
    color: WARNING_COLOR,
  ),
  repo.RsNetworkStatus.TIER_CONNECTED :  ConnectionStatus (
    statusMessage : 'You are fully connected!',
    explanation: 'Tier1 peer connected. Synchronization fully functional.', // todo(intl)
    icon: Icons.signal_cellular_4_bar,
    color: LOCAL_COLOR,
  ),
};

final connectionStatusProvider = StateProvider<ConnectionStatus>((ref) => statusStateMap[repo.RsNetworkStatus.NO_PEERS]);


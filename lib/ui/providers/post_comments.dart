
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:ui';

import 'package:elRepoIo/ui/providers/models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:elrepo_lib/repo.dart' as repo;

class PostCommentsProviderParams {
  String forumId, postId;
  PostCommentsProviderParams(this.forumId, this.postId);
  // Needed to use the provider properly and update it
  @override
  bool operator == (other) {
    return (other is PostCommentsProviderParams)
        && other.forumId == forumId
        && other.postId == postId;
  }
  @override
  int get hashCode => hashValues(forumId, postId);
}

final postCommentsProvider  = StateNotifierProvider
.family<PostCommentsNotifier, List<Map<String, dynamic>>, PostCommentsProviderParams>
  ((ref, params) {
    return PostCommentsNotifier(params.forumId, params.postId);
});

class PostCommentsNotifier extends UpdatableNotifier<List<Map<String, dynamic>>> {
  // You need to specify forum and post id
  String forumId, postId;

  PostCommentsNotifier(this.forumId, this.postId): super(<Map<String, dynamic>>[]);

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  @override
  Future<void> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      await repo.getForumComments(forumId, postId).then((value) {
        if (!listEquals(state, value)) state = value;
        alreadyUpdating = false;
      });
    }
  }
}
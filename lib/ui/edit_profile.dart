/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'package:elRepoIo/ui/common/loading_dialog.dart';
import 'package:elRepoIo/ui/providers/images_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show  defaultTargetPlatform, TargetPlatform;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:elRepoIo/constants.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart' show Identity, RsGxsImage;
import 'package:elrepo_lib/repo.dart' as repo;

import '../routes.dart';
import 'providers/identities_provider.dart';

class EditMyProfile extends ConsumerStatefulWidget {
  EditProfileArguments arguments;

  EditMyProfile(this.arguments, {Key key}) : super(key: key);

  @override
  _EditMyProfileState createState() => _EditMyProfileState();
}

class _EditMyProfileState extends ConsumerState<EditMyProfile> {
  // Image base64 from RS backend
  RsGxsImage _image;
  String _base64Image = '';

  // Picked image with the file picker
  final ImagePicker _picker = ImagePicker();

  final TextEditingController _teNewName = TextEditingController();

  /// Function that tell if with the option only avatar enabled, the avatar is
  /// changed.
  ///
  /// Util to enable/disable the save button. Return [true] when on onlyAvatar is
  /// [false] or when onlyAvatar is [true] and [_base64Image] is set.
  bool saveButtonActivated() =>
      !widget.arguments.onlyAvatar
          || (widget.arguments.onlyAvatar && _base64Image.isNotEmpty);

  @override
  void initState() {
    _base64Image = ref.read(ownIdentityProvider).avatar;
    super.initState();
  }

  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    final _identityData = ref.watch(ownIdentityProvider);
    return WillPopScope(
      onWillPop: () async => !widget.arguments.onlyAvatar, // Prevent back button on only avatar screen
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: PURPLE_COLOR.withOpacity(0.9),
          shadowColor: REMOTE_COLOR,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back_rounded,
              color: WHITE_COLOR,
            ),
          ),
          title: Text(
            'Edit Profile', // todo(intl)
            style: TextStyle(fontWeight: FontWeight.w800, color: WHITE_COLOR),
          ),
        ),
        body: Form(
          child: Container(
            padding: EdgeInsets.only(left: 16, top: 25, right: 16),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: ListView(
                children: [
                  /* Text(
                    "Edit Profile",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.w400),
                  ),
                  SizedBox(
                    height: 9,
                  ),*/
                  imageProfile(context),
                  SizedBox(
                    height: 27,
                  ),
                  if(widget.arguments.onlyAvatar)
                    Center(
                      child: Text(
                        'Set up your Avatar!', // todo(intl)
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: LOCAL_COLOR,
                        ),
                      ),
                    )
                  else if(!widget.arguments.onlyAvatar)
                    buildTextField(
                        'Full Name',
                        _identityData.name,
                        false,
                        _teNewName), //todo(intl)
                  // buildTextField("E-mail", "dylon@riseup.la", false),//todo(intl)
                  // buildTextField("Password", "....", true),//todo(intl)
                  // buildTextField("Location", "Berkeley California", false),//todo(intl)
                  SizedBox(
                    height: 80,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RaisedButton(
                        padding: EdgeInsets.symmetric(horizontal: 36),
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: REMOTE_COLOR,
                        child: Text('CANCEL', //todo(intl)
                            style: TextStyle(
                                fontSize: 14,
                                letterSpacing: 2.2,
                                color: Colors.white)),
                      ),
                      InkWell(
                        onTap: () {},
                        child: RaisedButton(
                          color:
                          saveButtonActivated()
                              ? LOCAL_COLOR : NEUTRAL_COLOR,
                          padding: EdgeInsets.symmetric(horizontal: 46),
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          onPressed: () async {
                            if(!saveButtonActivated()) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text("Select avatar or push cancel!"), // todo(intl)
                                    duration: Duration(seconds: 4),
                                  )
                              );
                              return;
                            }

                            var pr = ProgressDialog(context,
                                initialMessage:
                                'Updating your profile' // todo(intl)
                            )
                              ..show();

                            /// Create a RsGxsImage with the original avatar preventing new avatar is empty
                            var originalAvatar = _identityData.avatar.isNotEmpty
                                ? RsGxsImage.fromJson( {
                              'mSize': _identityData.avatarSize,
                              'mData': {'base64': _identityData.avatar},})
                                : null;

                            var res = await repo.updateOwnIdentity(
                                _identityData.mId,
                                _teNewName.text.isEmpty
                                    ? _identityData.name
                                    : _teNewName.text,
                                _image?.base64String?.isNotEmpty ?? false
                                    ? _image : originalAvatar);

                            var resMessage = "";
                            if (res != null) {
                              // Store updated info on the provider
                              ref.read(ownIdentityProvider.notifier).update();
                              // Update the image provider preventing user delete the avatar bug, that
                              // as is already cached, it don't show the "letters"
                              ref.read(imagesMapProvider.notifier).updateImage(res.mId, res.avatar);
                              resMessage = 'Profile updated'; // todo(intl)
                            } else {
                              resMessage = 'Error updating profile'; // todo(intl)
                            }

                            pr.hide(); // Pop dialog
                            final snackBar = SnackBar(
                              content: Text(resMessage),
                              duration: Duration(seconds: 6),
                            );
                            Navigator.pop(context);
                            ScaffoldMessenger.of(context).showSnackBar(
                                snackBar);
                          },
                          child: Text(
                            ' SAVE ', //todo(intl)
                            style: TextStyle(
                              fontSize: 14,
                              letterSpacing: 2.2,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget imageProfile(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          Container(
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                border: Border.all(
                    width: 4, color: Theme.of(context).scaffoldBackgroundColor),
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 2,
                      blurRadius: 10,
                      color: Colors.black.withOpacity(0.1),
                      offset: Offset(0, 10))
                ],
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: _base64Image == null || _base64Image.isEmpty
                      ? AssetImage('assets/logo.png')
                      : MemoryImage(base64.decode(_base64Image)),
                ),
              )),
          Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                    width: 2, color: Theme.of(context).scaffoldBackgroundColor),
                color: LOCAL_COLOR,
              ),
              child: InkWell(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: ((builder) => bottomSheet()),
                  );
                },
                child: Icon(
                  Icons.camera_alt_rounded,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget bottomSheet() {
    return Container(
      height: 110.0,
      width: 110.0,
      //width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          Text(
            'Choose Profile photo',
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            // Hide camera button because is not supported by web
            if(defaultTargetPlatform == TargetPlatform.android)
              FlatButton.icon(
                icon: Icon(Icons.camera),
                onPressed: () {
                  takePhoto(ImageSource.camera);
                },
                label: Text('Camera'),
              ),
            FlatButton.icon(
              icon: Icon(Icons.image),
              onPressed: () {
                takePhoto(ImageSource.gallery);
              },
              label: Text('Gallery'),
            ),
          ])
        ],
      ),
    );
  }

  void takePhoto(ImageSource source) async {
    var pickedFile = await _picker.pickImage(
      maxHeight: 250,
      maxWidth: 250,
      source: source,
    );
    Navigator.pop(context);
    if(pickedFile!=null) {
      _image = RsGxsImage(await pickedFile.readAsBytes());
      setState(() {
        _base64Image = _image.base64String;
      });
    }
  }

  Widget buildTextField(
      String labelText, String placeholder, bool isPasswordTextField,
      [TextEditingController controller]) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 26.0),
      child: TextField(
        obscureText: isPasswordTextField ? showPassword : false,
        controller: controller,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField
                ? IconButton(
              onPressed: () {
                setState(() {
                  showPassword = !showPassword;
                });
              },
              icon: Icon(
                showPassword
                    ? Icons.remove_red_eye
                    : Icons.visibility_off,
              ),
            )
                : null,
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            )),
      ),
    );
  }
}

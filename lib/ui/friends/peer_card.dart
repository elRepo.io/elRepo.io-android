/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/ui/providers/nodes_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/utils.dart';
import 'package:flutter/material.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/app_builder.dart';

/// Card used to show node details with some actions
///
/// The card show basic information from a node, friend or not. A `RsPeerDetails`
/// object or similar should be returned. The `RsPeerDetails` can accept an
/// `isConnected` attribute to show if is connected. By default false
///
/// Where [data] is the result of an `RsPeerDetails` API response.
///
/// [parentSetRemoteControlEnabled] is also a function to update parent setState
/// if you connect remotelly to a parent using remote control feature.
///
/// If [buttonsEnabled] is false, won't show buttons, only node info.
class PeerCard extends ConsumerStatefulWidget {
  String locationId;
  /// Invite data is used to show data got from an invite on the peer card. So
  /// we dont use location data from the locations provider
  Map<String, dynamic> inviteData;
  // Function(bool) parentSetRemoteControlEnabled;
  bool buttonsEnabled;

  PeerCard(this.locationId,
      {Key key,
        // this.parentSetRemoteControlEnabled,
        this.buttonsEnabled = true,
        this.inviteData,
      })
      : super(key: key);

  @override
  _PeerCardState createState() => _PeerCardState();
}

class _PeerCardState extends ConsumerState<PeerCard> {

  // State for remote control button
  // bool _supportRemoteControl = false;
  //
  // bool _isRemoteControllEnabled = false;

  bool _showAttemptConnectionDialog = true;

  /// Update nodesProvider
  void onrerun() => ref.watch(nodesProvider.notifier).update();

  Future<void> deleteNode(String gpgId) async => await ref.watch(nodesProvider.notifier).delete(gpgId);

  // void _checkRemoteControl() async {
  //   if (data['isConnected'] != null && data['isConnected'] && !_isRemoteControllEnabled) {
  //     _supportRemoteControl =
  //     await repo.supportRemoteControl(data['localAddr']);
  //     if (mounted) setState(() {});
  //   }
  // }

  // void _setRemoteControlEnabled(bool isEnabled) {
  //   widget.parentSetRemoteControlEnabled(isEnabled);
  //   setState(() {
  //     _isRemoteControllEnabled = isEnabled;
  //   });
  // }

  // void _enableRemoteControl() async {
  //   print('Enabling remote control for ' + data['localAddr']);
  //   await repo.enableRemoteControl(
  //       'http://' + data['localAddr'] + ':8888/rsremote',
  //       rs.RETROSHARE_API_USER)
  //       ? _setRemoteControlEnabled(true)
  //       : _setRemoteControlEnabled(false);
  //   if (_isRemoteControllEnabled) {
  //     AppBuilder.of(context).rebuild();
  //   }
  //   onrerun();
  // }

  @override
  void initState() {
    // Check if you are already connected to a remote host
    // _isRemoteControllEnabled =
    //     rs.getRetroshareServicePrefix() != rs.RETROSHARE_SERVICE_PREFIX;

    // Check if support remote control
    // _checkRemoteControl();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final data = widget.inviteData ?? ref.watch(nodeProvider(widget.locationId));
    // Prevent updating state of a deleted node. It tries to get the node but is
    // alreadt deleted
    if (data == null) return Container();
    if (data['isConnected'] == null) data['isConnected'] = false;
    return Card(
        elevation: 1.0,
        margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            leading: data['isConnected']
                ? Icon(Icons.phonelink_setup, color: LOCAL_COLOR)
                : Icon(Icons.phonelink_erase, color: RED_COLOR),
            title: Text(data['name'] + ' - ' + data['location']),
            subtitle: Column(
              children: [
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(getFormatedTime(data['lastConnect']))),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(data['id'])),
                if (widget.buttonsEnabled)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      if (!data['isConnected'])
                        Flexible(
                          child: TextButton.icon(
                            icon: Icon(Icons.tap_and_play, color: ACCENT_COLOR),
                            label: Text(
                              S.of(context).attemptconnection,
                              style: TextStyle(
                                color: ACCENT_COLOR,
                              ),
                            ),
                            onPressed: () {
                              print('Attempting connection ${data['id']}, show info dialog: $_showAttemptConnectionDialog');
                              rs.RsPeers.connectAttempt(data['id']);
                              if (_showAttemptConnectionDialog) {
                                showAttemptContectionDialog();
                              }
                              onrerun();
                            },
                          ),
                        ),
                      // else if (_supportRemoteControl)
                      //   Flexible(
                      //       child:TextButton.icon(
                      //         icon: Icon(Icons.settings_remote, color: LOCAL_COLOR),
                      //         label: _isRemoteControllEnabled
                      //             ? Text(
                      //           S.of(context).disconnect,
                      //           style: TextStyle(
                      //             color: ACCENT_COLOR,
                      //           ),
                      //         )
                      //             : Text(
                      //           S.of(context).remoteControl,
                      //           style: TextStyle(
                      //             color: ACCENT_COLOR,
                      //           ),
                      //         ),
                      //         onPressed: () {
                      //           _isRemoteControllEnabled
                      //               ? () {}
                      //               : _enableRemoteControl();
                      //         },
                      //       )),
                      const SizedBox(width: 8),
                      if(!repo.isTierOne(data['gpg_id']))
                        Flexible(
                          child: TextButton.icon(
                            icon: Icon(Icons.delete, color: RED_COLOR),
                            label: Text(
                              S.of(context).delete,
                              style: TextStyle(
                                color: RED_COLOR,
                              ),
                            ),
                            onPressed: () async {
                              await deleteNode(data['gpg_id']);
                            },
                          ),
                        )
                      else const SizedBox(height: 15,),

    ],
                  ),
              ],
            ),
          )
        ]));
  }

  void showAttemptContectionDialog()=>
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (context, setState) {
                return AlertDialog(
                  title: Text('Information'), // todo(intl)
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                          'This will attempt to connect to the node at the lasts known IP, '
                              'it may take some seconds. \n\n'
                              'If don\'t connect check your internet connection or try to add it again '
                              'from the "add friends screen" with an updated invite.'),
                      // todo(intl)
                      CheckboxListTile(
                        secondary: Text('Show this info again'),// todo(intl)
                        value: _showAttemptConnectionDialog,
                        onChanged: (bool value) {
                          setState(() {
                            _showAttemptConnectionDialog = value;
                          });
                        },
                      ),],
                  ),
                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("OK"),
                    )
                  ],
                );
              });
        },
      );
}

class _InnerCheckBox extends StatefulWidget {
  bool value;
  _InnerCheckBox(this.value, {Key key}) : super(key: key);

  @override
  _InnerCheckBoxState createState() => _InnerCheckBoxState();
}

class _InnerCheckBoxState extends State<_InnerCheckBox> {
  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      title: Text('Don\'t show this again'),// todo(intl)
      value: widget.value,
      onChanged: (bool value) {

        widget.value = !value;
        setState(() {
        });
      },
    );
  }
}

/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';

import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/ui/friends/peer_card.dart';
import 'package:elRepoIo/ui/providers/nodes_provider.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/app_builder.dart';
import 'package:elRepoIo/constants.dart';

class FriendNodesScreen extends ConsumerStatefulWidget {
  const FriendNodesScreen({Key key}) : super(key: key);

  static Route<dynamic> route() => MaterialPageRoute(
        builder: (context) => FriendNodesScreen(),
      );

  @override
  _FriendNodesScreenState createState() => _FriendNodesScreenState();
}

class _FriendNodesScreenState extends ConsumerState<FriendNodesScreen> {
  Timer _promiscuityTimer, _updateNodesTimer;
  bool _promisquity = true;

  // Check if you are already connected to a remote host
  // bool _isRemoteControllEnabled;

  Future<void> _checkPromiscuity(Timer t) async {
    if (t.isActive) {
      print('Executing promiscuity');
      var addedFriends = await repo.doPromiscuity();
      if (addedFriends.isNotEmpty) {
        print('Added new friends:' + addedFriends.toString());
        var peernames =
            List.from(addedFriends.map((e) => e['mProfileName'])).join(',');
        final snackBar = SnackBar(
          content: Text('Added friends: $peernames'),
          duration: Duration(seconds: 6),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }
  }

  void _setPromisquity(bool isPromiscuous) => isPromiscuous
      ? _promiscuityTimer = Timer.periodic(
          Duration(seconds: 2),
          _checkPromiscuity,
        )
      : _promiscuityTimer?.cancel();

  void _updateNodesProvider(WidgetRef ref, Timer t) {
    _updateNodesTimer ??= t;
    ref.watch(nodesProvider.notifier).update();
  }

  @override
  void dispose() {
    _promiscuityTimer?.cancel();
    _updateNodesTimer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    // Check if you are already connected to a remote host
    // _isRemoteControllEnabled =
    //     rs.getRetroshareServicePrefix() != rs.RETROSHARE_SERVICE_PREFIX;
    _setPromisquity(_promisquity);
    super.initState();
  }

  // Future<void> _disconnectRemoteControl() async {
  //   await repo.disableRemoteControl();
  //   _setRemoteControlEnabled(
  //       rs.getRetroshareServicePrefix() != rs.RETROSHARE_SERVICE_PREFIX);
  //   AppBuilder.of(context).rebuild();
  // }
  //
  // void _setRemoteControlEnabled(bool isEnabled) {
  //   setState(() {
  //     _isRemoteControllEnabled = isEnabled;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // todo: move all application AppBars to a model to DRY
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, '/addfriend');
          },
          backgroundColor: REMOTE_COLOR,
          child: Icon(Icons.add),
        ),
        appBar: AppBar(
          backgroundColor: PURPLE_COLOR.withOpacity(0.9),
          shadowColor: REMOTE_COLOR,
          brightness: Brightness.dark,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back_rounded,
              color: WHITE_COLOR,
            ),
          ),
          title: Text(
            S.of(context).friendNodesTitle,
            style: TextStyle(color: WHITE_COLOR),
          ),
        ),
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Theme.of(context).backgroundColor,
                pinned: false,
                expandedHeight: MediaQuery.of(context).size.height * .25,
                toolbarHeight: MediaQuery.of(context).size.height * .08,
                flexibleSpace: ListView(
                  shrinkWrap: true,
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 32,
                        vertical: 12,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Visibility(
                          //   visible: _isRemoteControllEnabled,
                          //   child: Column(
                          //     children: [
                          //       Text(
                          //         S.of(context).friendNodesConnected(
                          //             rs.getRetroshareServicePrefix()),
                          //         style: TextStyle(
                          //           color: LOCAL_COLOR,
                          //         ),
                          //       ),
                          //       TextButton(
                          //         style: ButtonStyle(
                          //           backgroundColor:
                          //               MaterialStateProperty.all(LOCAL_COLOR),
                          //         ),
                          //         onPressed: _disconnectRemoteControl,
                          //         child: Text(S.of(context).disconnect),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          SwitchListTile(
                            inactiveTrackColor: REMOTE_COLOR,
                            activeColor: LOCAL_COLOR,
                            contentPadding: const EdgeInsets.all(0),
                            value: _promisquity,
                            title: Text(
                              S.of(context).automaticpeering,
                            ),
                            onChanged: (val) {
                              setState(() {
                                _promisquity = !_promisquity;
                              });
                              _setPromisquity(_promisquity);
                            },
                          ),
                          Text(
                            S.of(context).peeringdescription,
                            style: TextStyle(
                              color: REMOTE_COLOR,
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .01,
                          ),
                          Divider(
                            height: MediaQuery.of(context).size.height * 0.006,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          Text(
                            S.of(context).knownnodes,
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ];
          },
          body: UpdatableListPage(
              nodesProvider,
              timerCallback: _updateNodesProvider,
              callbackWidget: (data) => PeerCard(
                data['id'],
                // parentSetRemoteControlEnabled: _setRemoteControlEnabled
              )
          ),
        ));
  }
}

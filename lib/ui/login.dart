/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart';
import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rsControl;
import 'package:elRepoIo/app_builder.dart';
import 'package:elRepoIo/ui/providers/autoupdate.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:elRepoIo/user_preferences.dart';
import 'package:flutter/foundation.dart' show defaultTargetPlatform, TargetPlatform;

import 'package:elRepoIo/ui/common/loading_dialog.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';

// class LoginScreen extends StatefulWidget {
//   final String locationName;
//   const LoginScreen({Key key, this.locationName = '' }) : super(key: key);
//
//   @override
//   State<LoginScreen> createState() => _LoginScreenState();
// }
//
// class _LoginScreenState extends State<LoginScreen> {
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Container(
//         child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Expanded(
//                 flex: 1,
//                 child: Container(
//                   decoration: BoxDecoration(
//                     image: DecorationImage(
//                       image: AssetImage('assets/bg1.jpg'),
//                       fit: BoxFit.cover,
//                       alignment: Alignment.bottomCenter,
//                     ),
//                   ),
//                 ),
//               ),
//               LoginForm()
//             ]),
//       ),
//     );
//   }
// }

class LoginScreen extends StatelessWidget {
  Map location;
  LoginScreen(this.location);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/bg1.jpg'),
                      fit: BoxFit.cover,
                      alignment: Alignment.bottomCenter,
                    ),
                  ),
                ),
              ),
              LoginForm(location: location,)
            ]),
      ),
    );
  }
}

class LoginForm extends ConsumerStatefulWidget {
  final Map location;
  LoginForm({Key key, this.location}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends ConsumerState<LoginForm> {
  bool _obscureText = true;
  //String _password;

  Map location;

  @override
  void initState() {
    widget.location == null ?
      rs.RsLoginHelper.getDefaultLocation().then((loc) {
      setState(() {
        location = loc;
      });
    }) : location = widget.location;
    super.initState();
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String password;
    return Form(
      key: _formKey,
      child: Expanded(
        flex: 3,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      S.of(context).loggingInAs,
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      location == null ? '' : location['mLocationName'],
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.lock,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: S.of(context).password,
                          suffix: InkWell(
                            onTap: _toggle,
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: LOCAL_COLOR,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return S.of(context).passwordCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => password = val,
                        obscureText: _obscureText,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child: RaisedButton(
                    onPressed: () async {
                      final form = _formKey.currentState;
                      if (form.validate()) {
                        var pr = ProgressDialog(context,
                            initialMessage: S.of(context).loggingIn)
                          ..show();
                        form.save();
                        final response = await doLogin(
                            ref, context, RsCredentials(location, password));
                        pr.hide(); //pop dialog
                        if(response != 0) {
                          final snackBar = SnackBar(
                            content: Text(
                                S.of(context).loginFailed),
                            duration: Duration(seconds: 6),
                          );
                          Scaffold.of(context).showSnackBar(snackBar);
                        } else {
                          Navigator.pushReplacementNamed(context, '/main');
                        }
                      }
                    },
                    color: LOCAL_COLOR,
                    textColor: Colors.white,
                    child: Center(child: Text(S.of(context).login)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

/// Function to do login
///
/// It return the status code from [repo.login]. If [creds] are different
/// than the stored on [USER_PREFERENCES.RS_CREDENTIALS] it store it again with
/// the new values.
///
/// It also execute some functions needed to execute after login, like
/// [repo.prepareSystem], [rsControl.initConnectivity], or Android specific
/// related functions.
///
/// [ref] Used to update [ownIdentityProvider]
// todo(kon): move this to el repolib or other side with app logic
Future<int> doLogin(WidgetRef ref, BuildContext context, RsCredentials creds) async {
  // If callback is not set, mean that maybe you pushed logout
  if(!rsControl.RsServiceControlPlatform.instance.isRsControllCallbackSet()) {
    rsControl.RsServiceControlPlatform.instance.setControlCallbacks();
  }
  final responseStatus = await repo.login(creds.secret, creds.location);
  if (responseStatus != 0) {
    print('Bad login attempt.');
  } else {
    if(defaultTargetPlatform == TargetPlatform.android) {
      // Create download and partials folder if don't exist and configure RS to use its
      // todo: this could be done once, executed when permisions are request for first time.
      // But, when you are restoring a RS account, the download directories are setup but couldn't
      // exist on system folders. So, for the moment, we have to execute it each time we login in.
      // We check if [RsFoldersControl.isStoragePermissionGranted] because don't ask it until
      // the user demnads it. For example, if we restored a backup, we already granted, and
      // we can do the `prepareSystemDirectories`. Instead, for a new account, we have to wait
      // until publish or download.
      // A way to fix this is to store on the users preferences if prepareSystemDirectories is
      // done.
      if(await RsFoldersControl.isStoragePermissionGranted()) {
        RsFoldersControl.prepareSystemDirectories();
      }
      // await RsFoldersControl.prepareSystemDirectories();
      if(USER_PREFERENCES.RS_CREDENTIALS != creds) setRsCredentials(creds.location, creds.secret);
    }
    // Start befriending with tiers, some caches...
    repo.prepareSystem();
    // Initialize the connectivity settings after RS login so we can set rates.
    rsControl.initConnectivity();
    // Do the first provider update
    updateProvidersList(ref, firstUpdateProviders);
  }
  return responseStatus;
}
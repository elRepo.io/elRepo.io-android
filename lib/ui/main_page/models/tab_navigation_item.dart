/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/routes.dart' show TabsPageArguments;
import 'package:elRepoIo/ui/main_page/pages/explore_page.dart';
import 'package:elRepoIo/ui/main_page/pages/bookmarks_page.dart';
import 'package:elRepoIo/ui/publish.dart';
import 'package:flutter/material.dart';
import 'package:elRepoIo/generated/l10n.dart';

/// In this class we define the main bottom icons which open `publish`, `local`,
/// and `discover` screens.
class TabNavigationItem {
  /// Return the widget page. The dynamic arg could be the arguments for the
  /// page or null.
  final Widget Function(TabsPageArguments) page;
  final String title;
  final Icon icon;
  /// Used to identify each tab
  final String key;
  dynamic arguments;

  TabNavigationItem({
    @required this.page,
    @required this.title,
    @required this.icon,
    @required this.key,
  });

  // todo(kon) : This should be a singleton
  static List<TabNavigationItem> get items => [
    TabNavigationItem(
      page: (args) =>  ExploreScreen(),
      icon: Icon(DISCOVER_ICON),
      title: S.current.tabsDiscover,
      key: 'discover',
    ),
    TabNavigationItem(
      page: (args) => PublishScreen(args: args?.publishFormArgs),
      icon: Icon(PUBLISH_ICON),
      title: S.current.publish,
      key: 'publish',
    ),
    TabNavigationItem(
      page: (args) => BookmarksScreen(),
      icon: Icon(Icons.local_library_rounded),
      title: S.current.tabsLocal,
      key: 'local',
    ),
  ];
}

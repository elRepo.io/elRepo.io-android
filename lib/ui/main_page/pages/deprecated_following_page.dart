/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/identity_card.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:flutter/material.dart';

class FollowingPage extends StatefulWidget {
  static Route<dynamic> route() => MaterialPageRoute(
        builder: (context) => FollowingPage(),
      );

  @override
  _FollowingPageState createState() => _FollowingPageState();
}

class _FollowingPageState extends State<FollowingPage> {
  Future<List<dynamic>> _getFollowedAuthorsDetails() =>
      repo.getFollowedAuthorsInfo();

  @override
  Widget build(BuildContext context) {
    return UpdatableFutureBuilder(
        generator: _getFollowedAuthorsDetails,
        callbackWidget: (context, data, updateFunction) => IdentityCard(data));
  }
}

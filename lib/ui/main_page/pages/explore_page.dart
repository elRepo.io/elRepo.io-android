/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/main_page/models/browse_tab_bar.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/ui/main_page/models/commons.dart';
import 'package:elRepoIo/ui/common/no_data_yet.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/ui/common/identity_card.dart';
import 'package:elRepoIo/ui/common/posts_teaser.dart';
import 'package:elRepoIo/ui/providers/identities_following.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:flutter/material.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

class ExploreScreen extends StatefulWidget {
  static Route<dynamic> route() => MaterialPageRoute(
        builder: (context) => ExploreScreen(),
      );

  @override
  _ExploreScreenState createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen> {

  /// Delay time each explore post page have to update (in seconds)
  final _exploreDelaySecs = 7;
  /// Explore page auto update iterations
  int _autoUpdateIterations = 0;

  /// Used to know if we want to automatically continue auto updating explore
  /// posts list page.
  ///
  /// It continue updating meanwhile posts list is empty, less than 20 or is
  /// updating for less than 60 seconds
  bool _autoUpdateExplore (List<RsMsgMetaData> posts) =>
      posts.isEmpty || (posts.length < 20 && (_exploreDelaySecs * _autoUpdateIterations) < 60) ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BrowseXScreen(myTabs: {
        S.current.tabsExplore: UpdatableListPage<RsMsgMetaData>(
          explorePostsProvider,
          backgroundDecoration: defaultBackground,
          callbackWidget: (data) => PostTeaserCard(data,),
          noDataWidget: (ref) => exploreNoData(),
          timerDelay: Duration(seconds: _exploreDelaySecs),
          timerCallback: (ref, timer) {
            if(_autoUpdateExplore(ref.read(explorePostsProvider))) {
              _autoUpdateIterations++;
              ref.watch(explorePostsProvider.notifier).update();
            }
            else {
              timer.cancel();
            }
          },
        ),
        S.current.tabsFollowing: UpdatableListPage(
          followingCardProvider,
          backgroundDecoration: defaultBackground,
          noDataWidget:(ref) => followingNoData(),
          callbackWidget: (data) =>  IdentityPostsScroll(data),
        ),
      }),
    );
  }
}

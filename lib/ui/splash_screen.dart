/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart';
import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rsControl;
import 'package:elRepoIo/ui/common/fancy_spinner.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/login.dart';
import 'package:flutter/foundation.dart' show defaultTargetPlatform, TargetPlatform;

import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/user_preferences.dart';
import 'package:elRepoIo/routes.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:permission_handler/permission_handler.dart';

import '../main.dart';

class SplashScreen extends ConsumerStatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends ConsumerState<SplashScreen> {
  Future<void> _userPrefs, _initialize;

  String _statusString = '';

  /// Initialize Retroshare and start retroshare loop
  ///
  /// For Android platform it also initialize downloads folder
  Future<void> _initializeRetroShare() async {
    await rsControl.initializeRetroShare(_startRetroShareErrorCallback);
  }

  Future<void> _loadUserPreferences() async {
    // Initialize previously stored user preferences like language or sentry
    await initializePreferences();
    // Show sentry dialog to request the user to send anonymous crash reports
    if(USER_PREFERENCES.SENTRY_SEND_REPORTS == null) await _showSentryDialog();
    // Configure auto login
    if(USER_PREFERENCES.RS_CREDENTIALS != null) await _doAutoLogin(USER_PREFERENCES.RS_CREDENTIALS);
  }

  Future<void> _doAutoLogin (RsCredentials creds) async {
    if(mounted) {
      setState(() {
        _statusString += 'Accessing to your account'; // todo(intl)
      });
      print('Doing login ${creds.toJson()}');
      final statusCode = await doLogin(ref, context, creds);
      print('Auto Login finished with status code $statusCode');
    }
  }

  /// It launch [redirectOnAccountStatus] in case all Futures are finished
  /// successfully. In this case the Futures are [_loadUserPreferences] and
  /// [_initializeRetroShare]
  Future<void> _redirectOnAccountStatus() async =>
      await Future.wait([_userPrefs, _initialize])
          .whenComplete(() => redirectOnAccountStatus(context));


  @override
  void initState() {
    super.initState();
    _userPrefs = _loadUserPreferences();
    _initialize = _initializeRetroShare();
    _redirectOnAccountStatus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(child: Image.asset('assets/logo.png')),
          Text(
            'elRepo.io\n',
            style: Theme.of(context).textTheme.headline4,
          ),
          FancySpinner(
            radius: 15.0,
            dotRadius: 6.0,),
          Visibility(
              visible: _statusString.isNotEmpty,
              child: OpacityBlinkingText(
                  Text(_statusString))
          ),
        ],
      ),
    );
  }

  /// Ask if you want to send anonymous data usage to our third party crash
  /// report service (sentry).
  ///
  /// This dialog is shown when in flutter local storage the sentry parameter
  /// is not set.
  Future<void> _showSentryDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Send anonymous crash reports'), // todo(intl)
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Send anonymous crash report to help to improve elRepo.io?'),// todo(intl)
                Text('We will send data to a trusted third party server: https://sentry.io/privacy'),// todo(intl)
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                setSentrySendReports(false);
                Navigator.pop(context, 'CANCEL');
              },
              child: Text(S.of(context).cancel),
            ),
            TextButton(
              onPressed: () {
                setSentrySendReports(true);
                Navigator.pop(context, 'OK');
              },
              child: Text('ACCEPT'), // todo(intl)
            ),
          ],
        );
      },
    );
  }

  /// Used to prevent showing multiple times [_startRetroShareErrorCallback]
  bool _dialogIsShown = false;

  /// Dialog showed when can't start retroshare
  Future<void> _startRetroShareErrorCallback() async {
    if(!_dialogIsShown) {
      _dialogIsShown = true;
      if(defaultTargetPlatform == TargetPlatform.android && !(await Permission.ignoreBatteryOptimizations.isGranted)) {
        await Permission.ignoreBatteryOptimizations.request();
        _dialogIsShown = false;
      } else {
        return showDialog<void>(
          context: navigatorKey.currentContext,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('RetroShare service seems down'), // todo(intl)
              content: SingleChildScrollView(
                child: ListBody(
                  children: const <Widget>[
                    Text('The retroshare-service seems down and is needed for elrepo.io to work'),// todo(intl)
                    Text('Please start it manually'),// todo(intl)
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    setSentrySendReports(true);
                    _dialogIsShown = false;
                    Navigator.pop(context, 'OK');
                  },
                  child: Text(S.of(context).close),
                ),
              ],
            );
          },
        );
      }
    }
  }
}

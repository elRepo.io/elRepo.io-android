/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/providers/connection_status_provider.dart';
import 'package:elRepoIo/ui/providers/timers_provider.dart';
import 'package:flutter/material.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/generated/l10n.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Widget that shows an icon depending on peers connection
///
/// Three status: connected to Tier one, connected to a peer but not to a Tier
/// and no peers connected.
// todo(kon) use [nodesProvider] for that
class ConnectionStatusIcon extends ConsumerStatefulWidget {
  const ConnectionStatusIcon({Key key}) : super(key: key);

  @override
  _ConnectionStatusIconState createState() => _ConnectionStatusIconState();
}

class _ConnectionStatusIconState extends ConsumerState<ConnectionStatusIcon> {
  Timer checkStatusTimer;

  void _checkNetworkStatus() async {
    ref.read(connectionStatusProvider.state).state = statusStateMap[await repo.checkRsNetworkStatus()];
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      checkStatusTimer = ref.read(timersProvider.notifier).addTimer(
          Timer.periodic(Duration(seconds: 3), (timer) {
            _checkNetworkStatus();
          }));
      _checkNetworkStatus();
    });
    super.initState();
  }

  @override
  void dispose() {
    checkStatusTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _currentStatus = ref.watch(connectionStatusProvider.state).state;
    return PopupMenuButton(
      offset: Offset(48, 48), // 24 is the icon default size
      tooltip: _currentStatus.statusMessage,
      icon: Icon(
        _currentStatus.icon,
        color: _currentStatus.color,
      ),
      itemBuilder: (BuildContext context) => <PopupMenuEntry>[
        PopupMenuItem(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                _currentStatus.statusMessage,
                style: TextStyle(color:  _currentStatus.color),
              ),
            ],
          ),
        ),
        PopupMenuItem(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  for(var status in statusStateMap.values)
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: Row(children: [
                        Icon(status.icon, size: 25, color: status.color,),
                        SizedBox(width: 10), // give it width
                        Flexible(child: Text(status.explanation,
                          style: TextStyle(color: REMOTE_COLOR, fontSize: 12),))
                      ]),
                    ),
                ])
        ),
        const PopupMenuDivider(),
        PopupMenuItem(
          child: ListTile(
            leading: Icon(
              Icons.share,
              color: LOCAL_COLOR,
            ),
            title: Text(
              S.of(context).network,
            ),
            trailing: Icon(
              Icons.keyboard_arrow_right_rounded,
              color: LOCAL_COLOR,
            ),
            onTap: () {
              Navigator.pushNamed(context, '/friendnodes');
            },
          ),
        ),
      ],
    );
  }
}

/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'dart:io';

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elrepo_lib/models.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';

/// Used to show an image that you can "open" if you click on it and styled size
/// and border.
///
/// Actually this is a preview for a local image, NOT a preview for a
/// publication metadata.
class ImagePreview extends StatelessWidget {
  final String filePath;
  const ImagePreview(this.filePath, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async{
        final openStatus = await OpenFile.open(filePath);
        print('Open file result: ${openStatus.message}');
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 15.0),
        width: 200,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft:    Radius.circular(5),
              topRight:   Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight:Radius.circular(5)
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.75),
              spreadRadius: 4,
              blurRadius: 5,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ],
        ),
        child: Image.file(
          File(filePath),
        ),
      ),
    );
  }
}

class B64Image extends StatelessWidget {
  final String base64Image;

  const B64Image(this.base64Image, {Key key}) : super(key: key);

  Image imageFromBase64String(String base64String) {
    return Image.memory(base64Decode(base64String), gaplessPlayback: true, fit: BoxFit.fill,);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: imageFromBase64String(base64Image),
    );
  }
}

class ConstrainedB64Image extends StatelessWidget {
  final String base64Image;
  final minHeight, minWidth, maxHeight, maxWidth;

  const ConstrainedB64Image(this.base64Image, {
    Key key,
    this.minWidth = 0.0,
    this.maxWidth = double.infinity,
    this.minHeight = 0.0,
    this.maxHeight = double.infinity,
  }) :  assert(minWidth != null),
        assert(maxWidth != null),
        assert(minHeight != null),
        assert(maxHeight != null) ,
        super(key: key) ;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        constraints: BoxConstraints(
          minHeight: minHeight,
          minWidth: minWidth,
          maxWidth: maxWidth,
          maxHeight: maxHeight,
        ),
        alignment: Alignment.topCenter,
        child: B64Image(base64Image),
      ),
    );
  }
}

/// Show contentype icon or instead show post preview
class IconOrPreview extends StatelessWidget {
  final PostMetadata postMetadata;
  final double defaultIconSize;
  final Color iconColor;
  const IconOrPreview(this.postMetadata, {
    Key key,
    this.defaultIconSize  = 24.0,
    this.iconColor = REMOTE_COLOR,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if(postMetadata?.hasPreview() ?? false)
          ConstrainedB64Image(postMetadata.preview.data, maxHeight: 60.0, maxWidth: 60.0, minWidth: 60.0,)
        else
          Icon(getContentTypeIcon(postMetadata?.contentType), color: iconColor, size: defaultIconSize,)
      ],
    );
  }
}



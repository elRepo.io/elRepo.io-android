/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/avatar_or_icon.dart';
import 'package:elRepoIo/ui/common/buttons.dart';
import 'package:elRepoIo/ui/common/posts_teaser.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/ui/providers/identities_following.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elrepo_lib/constants.dart' as cnst;
import 'package:elrepo_lib/models.dart';


import '../../constants.dart';

/// Identity card with a horizontal scroll with posts preview
class IdentityPostsScroll extends ConsumerStatefulWidget {
  final Identity data;
  const IdentityPostsScroll(this.data, {Key key}) : super(key: key);

  @override
  _IdentityPostsScrollState createState() => _IdentityPostsScrollState();
}

class _IdentityPostsScrollState extends ConsumerState<IdentityPostsScroll> {
  /// Used to wet the with of the post teaser card
  static const _cardWidth = 250.0;

  @override
  Widget build(BuildContext context) {
    var idProvider = identityPostsProvider(widget.data.mId);
    ref.watch(idProvider.notifier).update();
    return Column(
      children: [
        IdentityCard(widget.data),
        Container(
          height: MediaQuery.of(context).size.height * .25,
          child: UpdatableListPage<RsMsgMetaData>(
            idProvider,
            scrollDirection: Axis.horizontal,
            noDataWidget: (ref) => SynchronizingPostsButton(
                S.of(context).commonButtonsSynchronizePosts,
                onPressed: () => ref.watch(idProvider.notifier).update() ),
            callbackWidget: (data) =>  Container(
                width:  _cardWidth,
                child: PostTeaserCardBrief(data,)),
          ),
        )
      ],
    );
  }
}


class IdentityCard extends ConsumerWidget {
  Identity data;
  String _avatarData;
  String _nickname;
  String _mGroupId;

  IdentityCard(this.data) {
    _avatarData = data.avatar;
    _nickname = data.name;
    _mGroupId = data.mId;
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var publishedCount = ref.watch(identityPostsPublishedCount(data.mId));
    return Card(
      child: InkWell(
        onTap: () {
          print('tapped on $data}');
          Navigator.pushNamed(context, '/identityDetails',
              arguments: IdentityArguments(_mGroupId, data));
        },
        child: Container(
          padding: EdgeInsets.all(5.0),
          child: Column(
            children: <Widget>[
              ListTile(
                dense: true,
                leading: AvatarOrIcon(_avatarData, altText: _nickname, cacheKey: _mGroupId,),
                title: Text(
                  _nickname,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5),
                ),
                subtitle: Column(
                  children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: TimeText(data.mPublishTs.xint64)),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: IconTextWrap(
                        publishedCount >= 0
                            ? '$publishedCount publications' : '..'
                        , iconData: Icons.chat,), // todo(intl)
                    ),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Text(_mGroupId)),
                  ],
                ),
                // Disable flags to don't cause user confusion
                // trailing: data["flag"]["value"] == 7 ? Icon(Icons.verified) : Icon(Icons.pending)
              )
            ],
          ),
        ),
      ),
    );
  }
}

/// Little card with the avatar and the name or identity id in one line
class IdentityCardLittle extends StatelessWidget {
  final Identity data;
  final double fontSize;

  IdentityCardLittle(this.data, {this.fontSize = 17});

  @override
  Widget build(BuildContext context) {
    return
      Wrap(
          spacing: 5,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            if(data == null)
              Icon(
                Icons.person,
                color: LOCAL_COLOR,
                size: 12,
              )
            else AvatarOrIcon(
              data.avatar,
              altText: data.name,
              radius: 8,
              cacheKey: data.mId,
            ),
            if(data != null)
              Wrap(
                spacing: 2.5,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  Text(data.name ?? data.mId,
                      style: TextStyle(
                          color: LOCAL_COLOR, fontSize: fontSize)),
                  if(data.mId == rs.authIdentityId)
                    Text('(Me)', // todo(intl)
                        style: TextStyle(
                            color: LOCAL_COLOR, fontSize: fontSize-5)),
                ],
              )
            else Text('...',
                style: TextStyle(color: LOCAL_COLOR, fontSize: fontSize)),
          ]);
  }
}

/// Own identity card shown on settings screen that redirects to editprofile
/// screen onclick
///
/// Is a statefull widget to manage avatar changes
class OwnIdentityCard extends ConsumerStatefulWidget {
  const OwnIdentityCard({Key key}) : super(key: key);

  @override
  _OwnIdentityCardState createState() => _OwnIdentityCardState();
}

class _OwnIdentityCardState extends ConsumerState<OwnIdentityCard> {
  @override
  Widget build(BuildContext context) {
    final ownId = ref.watch(ownIdentityProvider);
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)),
      color: LOCAL_COLOR,
      child:
      ListTile(
        title: Text(
          ownId == null ? '' : ownId.name,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
        subtitle: Text(
          '${ownId.mId}',
          style: TextStyle(
              color: Colors.white,
              fontSize: 12,
              fontStyle: FontStyle.italic
            // fontWeight: FontWeight.w100,
          ),
        ),
        leading: ownId != null
            ? AvatarOrIcon(
          ownId.avatar,
          cacheKey: ownId.mId,
          altText: ownId.name,
        )
            : null,
        trailing: GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, '/addfriend');
          },
          child: Icon(Icons.share, color: WHITE_COLOR),
        ),
        onLongPress: () {
          copyToClipboard(context,  ownId.mId, 'Id copied!'); // todo(intl)
        },
        onTap: () {
          Navigator.pushNamed(context, '/editprofile',
              arguments: EditProfileArguments());
        },
      ),
    );
  }
}



// class OwnIdentityCard extends StatelessWidget {
//   const OwnIdentityCard({Key key}) : super(key: key);
//
// }

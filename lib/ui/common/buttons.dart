/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:flutter/material.dart';

/// Display classic no data yet button, grey with an icon of a clock
class SynchronizingPostsButton extends StatelessWidget {
  Function onPressed;
  String label;
  SynchronizingPostsButton( this.label,
      {@required this.onPressed, Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(0)),
        shadowColor: MaterialStateProperty.all(Colors.transparent),
        backgroundColor: MaterialStateProperty.all(Colors.transparent),
      ),
      label: Text( label, style: TextStyle(color: NEUTRAL_COLOR), ),
      icon: Icon(Icons.hourglass_bottom, color: NEUTRAL_COLOR,),
      onPressed: () async => onPressed(),
    );
  }
}

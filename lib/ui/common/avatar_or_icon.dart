/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/providers/images_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';



/// Icon that show classic CircleAvatar with two letter or an base64 image avatar
///
/// If [base64string] is empty will show two letter avatar
///
/// It implements a cache of [MemoryImage] to load the avatars faster. It has a
/// `Map<String, String>` where the firs String is the key [cacheKey] and the
/// value is the [base64string]. If exists, first load this image, then, compare
/// the existing base64image with the new one, asynchronous, to update it if
/// necessary.
///
/// If no [cacheKey] is defined, will not store/get anything on the cache.
class AvatarOrIcon extends ConsumerStatefulWidget {
  /// The base64string image for the avatar, can be empty
  final String base64string;
  /// For example the nickname where to extract the showing letters if base64 is
  /// empty
  final String altText;
  /// Default value for radius is 20.0
  final double radius;
  /// This key is used to store/get the base64image in the avatarCaches
  final String cacheKey;

  const AvatarOrIcon(this.base64string,
      {Key key, this.altText, this.radius = 20.0, this.cacheKey = ''}) : super(key: key);

  @override
  _AvatarOrIconState createState() => _AvatarOrIconState();
}

class _AvatarOrIconState extends ConsumerState<AvatarOrIcon> {

  @override
  void initState() {
    if(widget.cacheKey.isNotEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(imagesMapProvider.notifier).updateIfOutdated(widget.cacheKey, widget.base64string);
      });
    }
    super.initState();
  }


  Widget _circleAvatarOrTxtAvatar([memoryImage]) {
    return memoryImage != null
        ? CircleAvatar(
        radius: widget.radius,
        backgroundImage: memoryImage)
        : CircleAvatar(
        radius: widget.radius,
        backgroundColor: LOCAL_COLOR,
        child: widget.altText.isNotEmpty
            ? Text(
          widget.radius > 20 && widget.altText.length > 1
              ? widget.altText.substring(0, 2).toUpperCase()
              : widget.altText.substring(0, 1).toUpperCase(),
          style: TextStyle(
              fontWeight: FontWeight.w800, color: WHITE_COLOR),
        )
            : Icon(Icons.account_circle));
  }

  @override
  Widget build(BuildContext context, ) {
    final _loadedAvatar = ref.watch(imagesMapProvider.select(
            (value) => value[widget.cacheKey]?.memoryImage));
    return widget.cacheKey.isNotEmpty
        ? _circleAvatarOrTxtAvatar(_loadedAvatar)
        // Case no cache key provided
        : _circleAvatarOrTxtAvatar(ImageAndBase64.generateMemoryImage(widget.base64string));
  }
}

/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

String _dialogMessage = 'Loading ...';
enum ProgressDialogType { Normal, Download }
ProgressDialogType _progressDialogType = ProgressDialogType.Normal;
double _progress = 0.0;
bool _isShowing = false;

/// Show updatable dialog with a message or a progress bar.
///
/// The [barrierDismissible] argument is used to indicate whether tapping on the
/// barrier will dismiss the dialog. It is true by default and can not be null.
/// At same time, [popOnBackButton] prevents the user to close the popup using
/// back button
///
/// [initialMessage] is used to change the first message to show. Or you can
/// change it after instantiate using [setMessage]. You can show and hide the
/// dialog using [show] and [hide] functions.
///
/// To update the information message use [update].
///
/// I didn't test progress bar support, this code is little bit modified from:
/// https://github.com/fabiojansenbr/codefirst_progress_dialog/
class ProgressDialog {
  _MyDialog _dialog;
  BuildContext _buildContext, _context;
  bool barrierDismissible, popOnBackButton;

  ProgressDialog(
    BuildContext buildContext, {
    ProgressDialogType progressDialogtype = ProgressDialogType.Normal,
    String initialMessage,
    this.barrierDismissible = false,
    this.popOnBackButton = false,
  }) {
    _buildContext = buildContext;
    _progressDialogType = progressDialogtype;
    _setMessage(initialMessage);
  }

  void _setMessage(String mess) {
    _dialogMessage = mess;
    debugPrint('ProgressDialog message changed: $mess');
  }

  void update({double progress, String message}) {
    debugPrint('ProgressDialog message changed: ');
    if (_progressDialogType == ProgressDialogType.Download) {
      debugPrint('Old Progress: $_progress, New Progress: $progress');
      _progress = progress;
    }
    debugPrint('Old message: $_dialogMessage, New Message: $message');
    _dialogMessage = message;
    _dialog.update();
  }

  bool isShowing() {
    return _isShowing;
  }

  void hide() {
    _isShowing = false;
    Navigator.of(_buildContext).pop();
    print('ProgressDialog dismissed');
  }

  void show() {
    _dialog = _MyDialog();
    _isShowing = true;
    print('ProgressDialog shown');
    showDialog<dynamic>(
      context: _buildContext,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        _context = context;
        return WillPopScope(
            onWillPop: () async =>
                popOnBackButton, // This will prevent to close dialog with back button during the execution
            child: SimpleDialog(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                children: [_dialog]));
      },
    );
  }
}

// ignore: must_be_immutable
class _MyDialog extends StatefulWidget {
  final _dialog = _MyDialogState();
  update() {
    _dialog.changeState();
  }

  @override
  // ignore: must_be_immutable
  State<StatefulWidget> createState() {
    return _dialog;
  }
}

class _MyDialogState extends State<_MyDialog> {
  changeState() {
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    _isShowing = false;
  }

  @override
  Widget build(BuildContext context) {
    return
      // _progressDialogType == ProgressDialogType.Normal ?
      Center(
            child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              CircularProgressIndicator(),
              SizedBox(
                height: 10,
              ),
              Text(_dialogMessage,
                  // textAlign: TextAlign.justify,
                  style: TextStyle(color: ACCENT_COLOR))
            ]),
          ));
        // Dialog of type progress indicator, not tested
        // : Stack(
        //     children: <Widget>[
        //       Positioned(
        //         top: 35.0,
        //         child: Text(_dialogMessage,
        //             style: TextStyle(
        //                 color: Colors.black,
        //                 fontSize: 22.0,
        //                 fontWeight: FontWeight.w700)),
        //       ),
        //       Positioned(
        //         bottom: 15.0,
        //         right: 15.0,
        //         child: Text("$_progress/100",
        //             style: TextStyle(
        //                 color: Colors.black,
        //                 fontSize: 15.0,
        //                 fontWeight: FontWeight.w400)),
        //       ),
        //     ],
        //   );
  }
}

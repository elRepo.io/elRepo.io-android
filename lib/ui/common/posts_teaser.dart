/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/circles/circle_card.dart';
import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/identity_card.dart';
import 'package:elRepoIo/ui/common/image_preview.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:elrepo_lib/models.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart' show Identity, RsMsgMetaData;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// Used to show the publication title with a leading icon, depending if is
/// own publication, bookmarked, etc
class _TitleWithIcon extends ConsumerWidget {
  final TextStyle textStyle;
  final String text, forumId, postId, authorId;
  const _TitleWithIcon(this.text, this.forumId, this.postId, this.authorId, {Key key, this.textStyle}) : super(key: key);
  final double _badgeIconSize = 20.0; // Size of the badge icon that indicate if is bookmarked

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return RichText(
      text: TextSpan(children: [
        if(repo.isBookmarkedSync(forumId, postId))
          WidgetSpan(child: Icon(ICON_BOOKMARKED_TRUE, color: LOCAL_COLOR, size: _badgeIconSize,))
        else if(authorId == ref.read(ownIdentityProvider.notifier).state.mId)
          WidgetSpan(child: Icon(ICON_YOUR_CONTENT, color: LOCAL_COLOR, size: _badgeIconSize,)),
        TextSpan(
          text: text ?? '',
          style: textStyle ?? DefaultTextStyle.of(context).style,),
      ]),
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
      softWrap: true,
    );
  }
}


/// Normal [PostTeaserCard] tipically used on the [UpdatableListPage] to show
/// bookmarks, user forums etc...
class PostTeaserCard extends ConsumerWidget {
  RsMsgMetaData data;
  PostMetadata _postMetadata;
  String forumId, postId;

  PostTeaserCard(this.data,) {
    _postMetadata = data.postMetadata;
    if (_postMetadata?.referred != null && _postMetadata.referred.isNotEmpty) {
      var realPostIds = _postMetadata.referred.split(' ');
      forumId = realPostIds[0];
      postId = realPostIds[1];
    } else {
      forumId = data.mGroupId;
      postId = data.mMsgId;
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Card(
      elevation: 1.0,
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                leading: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide(width: 1.0, color: REMOTE_COLOR))),
                  child: IconOrPreview(_postMetadata),

      ),
                onTap: () {
                  // if it is a linkPost, we need to redirect to the CONTENT forum post on click.
                  var pa = PostArguments(forumId, postId, postMetadata: data);
                  debugPrint('Showing post details for ${data.mGroupId} with message id ${data.mMsgId}');
                  Navigator.pushNamed(context, '/postdetails', arguments: pa);
                },
                title: _TitleWithIcon(
                    _postMetadata.title,
                    forumId, postId, data.mAuthorId
                ),
                subtitle: Padding(
                  padding: EdgeInsets.only(left: 2.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TimeText(data.mPublishTs.xint64),
                      IdentityCardLittle(repo.getCachedIdentity(data.mAuthorId), fontSize: 12,),
                      // todo(kon): Add space and style it
                      if (_postMetadata.circle != null)
                        LittleCircleCard(
                            repo.getSubscribedCircleCached(_postMetadata.circle)['mGroupName']
                                ?? _postMetadata.circle),
                    ],
                  ),
                )
            ),
            if(_postMetadata.summary != null && _postMetadata.summary.isNotEmpty)
              Padding(
                padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
                child: Text(
                  _postMetadata.summary ?? '',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  textAlign: TextAlign.start,
                ),
              ),
          ],
        ),
      ),
    );
  }
}

/// Refactor of the [PostTeaserCard] used to show post metadata for identity
/// posts screen ([IdentityPostsScroll]) with the horizontal scroll.
class PostTeaserCardBrief extends ConsumerWidget {
  final RsMsgMetaData data;
  PostMetadata _postMetadata;
  String forumId, postId;

  PostTeaserCardBrief(this.data, {Key key}) : super(key: key) {
    _postMetadata = data.postMetadata;
    if (_postMetadata.referred != null) {
      var realPostIds = _postMetadata.referred.split(' ');
      forumId = realPostIds[0];
      postId = realPostIds[1];
    } else {
      forumId = data.mGroupId;
      postId = data.mMsgId;
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return GestureDetector(
      onTap: () {
        // if it is a linkPost, we need to redirect to the CONTENT forum post on click.
        var pa = PostArguments(forumId, postId, postMetadata: data);
        debugPrint('Showing post details for ${data.mGroupId} with message id ${data.mMsgId}');
        Navigator.pushNamed(context, '/postdetails', arguments: pa);
      },
      child: Card(
        elevation: 1.0,
        margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                        leading: Container(
                          padding: EdgeInsets.only(right: 12.0),
                          decoration: BoxDecoration(
                              border: Border(
                                  right: BorderSide(width: 1.0, color: REMOTE_COLOR))),
                          child: IconOrPreview(_postMetadata)
                        ),
                        subtitle: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TimeText(data.mPublishTs.xint64),
                            IdentityCardLittle(repo.getCachedIdentity(data.mAuthorId), fontSize: 12,),
                            // todo(kon): Add space and style it
                            if (_postMetadata.circle != null)
                              LittleCircleCard(
                                  repo.getSubscribedCircleCached(_postMetadata.circle)['mGroupName']
                                      ?? _postMetadata.circle),
                            // Text(_postMetadata.summary ?? ''),
                          ],
                        )
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Flexible(
                            child: Padding(
                              padding: EdgeInsets.only(left: 12.0, right: 10.0,),
                              child: _TitleWithIcon(
                                _postMetadata.title,
                                forumId, postId, data.mAuthorId,
                                textStyle: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: EdgeInsets.only(left: 12.0, right: 10.0, bottom: 7.0, top: 5.0),
                              child: Text(_postMetadata.summary ?? '',
                                  overflow: TextOverflow.fade,
                                  style: Theme.of(context).textTheme.bodyText2
                                      .copyWith(color: Colors.black54
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


// todo(kon): move this as a mixin of UpdatableFutureBuilder
class PostTeasersUpdatable extends StatefulWidget {
  final Function generator;
  final UpdatableFutureBuilderController controller;
  final Widget refreshButton;
  final Widget loadingBox;
  final bool enableLoadingBox;

  const PostTeasersUpdatable(
      {Key key, this.generator, this.controller,
        this.refreshButton, this.loadingBox, this.enableLoadingBox})
      : super(key: key);

  @override
  _PostTeasersUpdatable createState() => _PostTeasersUpdatable();
}

class _PostTeasersUpdatable extends State<PostTeasersUpdatable> {
  @override
  Widget build(BuildContext context) {
    return UpdatableFutureBuilder(
      controller: widget.controller,
      generator: widget.generator,
      refreshButton: widget.refreshButton,
      loadingBox: widget.loadingBox,
      enableLoadingBox: widget.enableLoadingBox,
      callbackWidget: (context, data, updateFunction) =>
          PostTeaserCard(data,),
    );
  }
}


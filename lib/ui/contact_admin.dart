import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:elRepoIo/constants.dart';

// todo: this is not used
class ContactAdmin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Contacts(),
    );
  }
}

class Contacts extends StatefulWidget {
  @override
  _ContactsState createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  List names = ['juana perez', 'juanito peres', 'juana perez2', 'juanito3'];
  List designations = [
    'data scientist',
    'developer',
    'software engineer',
    'hacktivist'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('contact admin'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: 4,
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) => Container(
            width: MediaQuery.of(context).size.width * 0.7,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
              ),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.2,
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.15,
                            height: MediaQuery.of(context).size.height * 0.075,
                            child: CircleAvatar(
                              backgroundColor: LOCAL_COLOR,
                              foregroundColor: LOCAL_COLOR,
                              backgroundImage: NetworkImage(
                                  'https://thispersondoesnotexist.com/image'),
                            ),
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                names[index],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(designations[index],
                                  style: TextStyle(color: NEUTRAL_COLOR)),
                            ],
                          ),
                        ],
                      ),
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.symmetric(
                            horizontal: 8.0, vertical: 8.0),
                        child: FlatButton(
                          onPressed: () {},
                          color: LOCAL_COLOR,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Text(
                            'Invite',
                            style: TextStyle(color: WHITE_COLOR),
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

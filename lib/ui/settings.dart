/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/identity_card.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/providers/theme_state.dart';
import 'package:elRepoIo/ui/providers/timers_provider.dart';
import 'package:elRepoIo/user_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rscontrol;
import 'package:flutter/foundation.dart' show defaultTargetPlatform, TargetPlatform;

import 'package:elrepo_lib/repo.dart' as repo;

import 'package:elRepoIo/ui/common/language_codes.dart' as langsMap;
import 'package:elRepoIo/app_builder.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

class SettingsProfile extends ConsumerStatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends ConsumerState<SettingsProfile> {
  String valueChoose;
  Identity identityInfo;

  @override
  void initState() {
    repo.getOwnIdentityDetails().then((identity) {
      setState(() {
        identityInfo = identity;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        //iconTheme: IconThemeData(color: Colors.black),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Text(
          S.of(context).settings,
          style: TextStyle(
            fontWeight: FontWeight.w800,
            color: WHITE_COLOR,
          ),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                OwnIdentityCard(),
                const SizedBox(height: 6.0),
                Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(
                        Icons.share,
                        color: LOCAL_COLOR,
                      ),
                      title: Text(
                        S.of(context).network,
                      ),
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/friendnodes');
                      },
                    ),
                    buildDivider(),
                    // ListTile(
                    //   leading: Icon(
                    //     Icons.animation,
                    //     color: LOCAL_COLOR,
                    //   ),
                    //   title: Text(
                    //       '${S.of(context).your} ${S.of(context).circlesTitle} '),
                    //   trailing: Icon(
                    //     Icons.keyboard_arrow_right_rounded,
                    //     color: LOCAL_COLOR,
                    //   ),
                    //   onTap: () {
                    //     Navigator.pushNamed(context, '/showCircles');
                    //   },
                    // ),
                    // if(defaultTargetPlatform == TargetPlatform.android) buildDivider(),
                    if(defaultTargetPlatform == TargetPlatform.android)
                      ListTile(
                        leading: Icon(
                          Icons.file_download,
                          color: LOCAL_COLOR,
                        ),
                        title: Text(
                          S.of(context).backup,
                        ),
                        trailing: Icon(
                          Icons.keyboard_arrow_right_rounded,
                          color: LOCAL_COLOR,
                        ),
                        onTap: () {
                          Navigator.pushNamed(context, '/createbackup');
                        },
                      ),
                    buildDivider(),
                    ListTile(
                      leading: Icon(
                        Icons.translate,
                        color: LOCAL_COLOR,
                      ),
                      title: DropdownButton(
                        hint: Text(
                          S.of(context).language,
                        ),
                        icon: Icon(
                          Icons.keyboard_arrow_down_rounded,
                          color: LOCAL_COLOR,
                        ),
                        isExpanded: true,
                        underline: SizedBox(),
                        value: valueChoose,
                        onChanged: (newValue) async {
                          setPreferredLanguage(newValue);
                          AppBuilder.of(context).rebuild();
                        },
                        items:
                        S.delegate.supportedLocales.map((Locale valueItem) {
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(langsMap
                                .isoLangs[valueItem.languageCode]['nativeName']
                                .split(',')[0]),
                          );
                        }).toList(),
                      ),
                      onTap: () {
                        //Navigator.pushNamed(context, '/language');
                      },
                    ),
                    buildDivider(),
                    ListTile(
                      leading: Icon(
                        Icons.privacy_tip,
                        color: LOCAL_COLOR,
                      ),
                      title: Text(S.of(context).appIntro),
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/nodatayet');
                      },
                    ),
                    buildDivider(),
                    ListTile(
                      leading: Icon(
                        Icons.science,
                        color: LOCAL_COLOR,
                      ),
                      title: Text('Experimental'), // todo(intl)
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        showModalBottomSheet<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return _experimentalMenu();
                          },
                        );
                      },
                    ),
                    buildDivider(),
                  ],
                ),
                const SizedBox(
                  height: 30.0,
                ),
                Text(
                  S.of(context).appSettings,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: LOCAL_COLOR,
                  ),
                ),
                const SizedBox(height: 6.0),
                Column(
                  children: <Widget>[
                    Dark(),
                    SwitchListTile(
                      inactiveTrackColor: REMOTE_COLOR,
                      activeColor: LOCAL_COLOR,
                      contentPadding: const EdgeInsets.all(0),
                      value: false,
                      title: Text(S.of(context).notifications),
                      onChanged: null,
                    ),
                    SwitchListTile(
                      inactiveTrackColor: REMOTE_COLOR,
                      activeColor: LOCAL_COLOR,
                      contentPadding: const EdgeInsets.all(0),
                      value: USER_PREFERENCES.SENTRY_SEND_REPORTS,
                      title: Text('Send anonymous crash reports'), // todo(intl)
                      onChanged: (value) async {
                        setSentrySendReports(value);
                        print('sentrySendData changed to: ${USER_PREFERENCES.SENTRY_SEND_REPORTS}');
                        setState(() { });
                      },
                    ),
                  ],
                ),
                const SizedBox(height: 6.0),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .02,
                ),
                Center(
                  child: RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 98),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    onPressed: () async {
                      simpleProgressDialog(
                          context,
                          children: [
                            Text('Stopping elRepo.io') // todo(intl)
                          ]
                      );
                      deleteRsCredentials();
                      ref.read(timersProvider.notifier).cancelAll(); // Cancel all global timers
                      await Future.delayed(Duration(seconds: 1)); // Await navigator to stop RS execution
                      // Disable restart loop
                      await rscontrol.RsServiceControlPlatform.instance.stopRetroShareExecution();
                      popUntilLogin(context,); // Pop until login to dispose all timers that are not set on global timer
                    },
                    color: LOCAL_COLOR,
                    child: Text(S.of(context).signOut,
                        style:
                        TextStyle(letterSpacing: 2.2, color: Colors.white)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _experimentalMenu(){
    return SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(
                height: 30.0,
              ),
              Text(
                'Experimental features', // todo(intl)
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: LOCAL_COLOR,
                ),
              ),
              const SizedBox(
                height: 30.0,
              ),
              ListTile(
                leading: Icon(
                  Icons.settings_remote,
                  color: LOCAL_COLOR,
                ),
                title: Text(S.of(context).remoteCtrl),
                trailing: Icon(
                  Icons.keyboard_arrow_right_rounded,
                  color: LOCAL_COLOR,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/remotecontrol');
                },
              ),
              buildDivider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: StatefulSwitchTile(
                  'Activate \"search similar\"', // todo(intl)
                  initialValue: USER_PREFERENCES.ACTIVATE_PERCEPTUAL_SEARCH,
                  secondary: Icon(
                    Icons.image_search,
                    color: LOCAL_COLOR,
                  ),
                  onChanged: (value) async {
                    setPerceptualSearch(value);
                    print('ACTIVATE_PERCEPTUAL_SEARCH changed to: '
                        '${USER_PREFERENCES.ACTIVATE_PERCEPTUAL_SEARCH}');
                    setState(() {});
                  },
                ),
              ),
              const SizedBox(
                height: 30.0,
              ),
            ]
        )
    );
  }
}

bool darkThemeEnabled = false;

class Dark extends ConsumerStatefulWidget {
  const Dark({
    Key key,
  }) : super(key: key);

  @override
  _DarkState createState() => _DarkState();
}

class _DarkState extends ConsumerState<Dark> {
  @override
  Widget build(BuildContext context) {
    final darkThemeProvider = ref.watch(themeNotifierProvider);
    return SwitchListTile(
      inactiveTrackColor: REMOTE_COLOR,
      activeColor: LOCAL_COLOR,
      contentPadding: const EdgeInsets.all(0),
      title: Text(S.of(context).darkMode),
      value: darkThemeProvider.isDarkThemeEnabled,
      onChanged: (_) {
        darkThemeProvider.setDarkTheme(!darkThemeProvider.isDarkThemeEnabled);
      },
    );
  }
}
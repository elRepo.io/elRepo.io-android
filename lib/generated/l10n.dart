// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Local`
  String get tabsLocal {
    return Intl.message(
      'Local',
      name: 'tabsLocal',
      desc: '',
      args: [],
    );
  }

  /// `Tags`
  String get tabsTags {
    return Intl.message(
      'Tags',
      name: 'tabsTags',
      desc: '',
      args: [],
    );
  }

  /// `Explore`
  String get tabsExplore {
    return Intl.message(
      'Explore',
      name: 'tabsExplore',
      desc: '',
      args: [],
    );
  }

  /// `Following`
  String get tabsFollowing {
    return Intl.message(
      'Following',
      name: 'tabsFollowing',
      desc: '',
      args: [],
    );
  }

  /// `Discover`
  String get tabsDiscover {
    return Intl.message(
      'Discover',
      name: 'tabsDiscover',
      desc: '',
      args: [],
    );
  }

  /// `Your content`
  String get tabsYourContent {
    return Intl.message(
      'Your content',
      name: 'tabsYourContent',
      desc: '',
      args: [],
    );
  }

  /// `Downloaded`
  String get tabsDownloaded {
    return Intl.message(
      'Downloaded',
      name: 'tabsDownloaded',
      desc: '',
      args: [],
    );
  }

  /// `In Progress`
  String get tabsInProgress {
    return Intl.message(
      'In Progress',
      name: 'tabsInProgress',
      desc: '',
      args: [],
    );
  }

  /// `bring down the cloud`
  String get onboardDownCloud {
    return Intl.message(
      'bring down the cloud',
      name: 'onboardDownCloud',
      desc: '',
      args: [],
    );
  }

  /// `Avoid censorship`
  String get onboardAvoidCensorship {
    return Intl.message(
      'Avoid censorship',
      name: 'onboardAvoidCensorship',
      desc: '',
      args: [],
    );
  }

  /// `Share your content without depending on information silos controlled by global monopolies`
  String get onboardAvoidCensorshipText {
    return Intl.message(
      'Share your content without depending on information silos controlled by global monopolies',
      name: 'onboardAvoidCensorshipText',
      desc: '',
      args: [],
    );
  }

  /// `Pop the information bubble`
  String get onboardPopBubble {
    return Intl.message(
      'Pop the information bubble',
      name: 'onboardPopBubble',
      desc: '',
      args: [],
    );
  }

  /// `What you search is what you get. No intrusive AI or algorithms will get between you and content shared by others`
  String get onboardPopBubbleText {
    return Intl.message(
      'What you search is what you get. No intrusive AI or algorithms will get between you and content shared by others',
      name: 'onboardPopBubbleText',
      desc: '',
      args: [],
    );
  }

  /// `Local, global, online and offline`
  String get onboardLocalGlobal {
    return Intl.message(
      'Local, global, online and offline',
      name: 'onboardLocalGlobal',
      desc: '',
      args: [],
    );
  }

  /// `Publish and view content even when you are not connected to the Internet.`
  String get onboardLocalGlobalText {
    return Intl.message(
      'Publish and view content even when you are not connected to the Internet.',
      name: 'onboardLocalGlobalText',
      desc: '',
      args: [],
    );
  }

  /// `Welcome to elRepo.io "glocal" family!`
  String get onboardFamily {
    return Intl.message(
      'Welcome to elRepo.io "glocal" family!',
      name: 'onboardFamily',
      desc: '',
      args: [],
    );
  }

  /// `You can now begin sharing and exploring content.`
  String get onboardFamilyText {
    return Intl.message(
      'You can now begin sharing and exploring content.',
      name: 'onboardFamilyText',
      desc: '',
      args: [],
    );
  }

  /// `Publication languages`
  String get languageDropdownHint {
    return Intl.message(
      'Publication languages',
      name: 'languageDropdownHint',
      desc: '',
      args: [],
    );
  }

  /// `Nothing selected`
  String get languageDropdownNothingSelected {
    return Intl.message(
      'Nothing selected',
      name: 'languageDropdownNothingSelected',
      desc: '',
      args: [],
    );
  }

  /// `elRepo.io network`
  String get friendNodesTitle {
    return Intl.message(
      'elRepo.io network',
      name: 'friendNodesTitle',
      desc: '',
      args: [],
    );
  }

  /// `Connetected to: {url}`
  String friendNodesConnected(Object url) {
    return Intl.message(
      'Connetected to: $url',
      name: 'friendNodesConnected',
      desc: '',
      args: [url],
    );
  }

  /// `elRepo.io`
  String get elRepoio {
    return Intl.message(
      'elRepo.io',
      name: 'elRepoio',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `No results`
  String get noResults {
    return Intl.message(
      'No results',
      name: 'noResults',
      desc: '',
      args: [],
    );
  }

  /// `Progress: {number} %`
  String progresPercent(Object number) {
    return Intl.message(
      'Progress: $number %',
      name: 'progresPercent',
      desc: '',
      args: [number],
    );
  }

  /// `{number}% match`
  String matchPercent(Object number) {
    return Intl.message(
      '$number% match',
      name: 'matchPercent',
      desc: '',
      args: [number],
    );
  }

  /// `Searching over elrepo.io network`
  String get searchOverRepoio {
    return Intl.message(
      'Searching over elrepo.io network',
      name: 'searchOverRepoio',
      desc: '',
      args: [],
    );
  }

  /// `Search similar`
  String get searchSimilar {
    return Intl.message(
      'Search similar',
      name: 'searchSimilar',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `ok`
  String get ok {
    return Intl.message(
      'ok',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get edit {
    return Intl.message(
      'Edit',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get error {
    return Intl.message(
      'Error',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  /// `Participants`
  String get participants {
    return Intl.message(
      'Participants',
      name: 'participants',
      desc: '',
      args: [],
    );
  }

  /// `Leave`
  String get leave {
    return Intl.message(
      'Leave',
      name: 'leave',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get create {
    return Intl.message(
      'Create',
      name: 'create',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get cont {
    return Intl.message(
      'Continue',
      name: 'cont',
      desc: '',
      args: [],
    );
  }

  /// `Your`
  String get your {
    return Intl.message(
      'Your',
      name: 'your',
      desc: '',
      args: [],
    );
  }

  /// `Details`
  String get details {
    return Intl.message(
      'Details',
      name: 'details',
      desc: '',
      args: [],
    );
  }

  /// `Circles`
  String get circlesTitle {
    return Intl.message(
      'Circles',
      name: 'circlesTitle',
      desc: '',
      args: [],
    );
  }

  /// `Circle`
  String get circle {
    return Intl.message(
      'Circle',
      name: 'circle',
      desc: '',
      args: [],
    );
  }

  /// `with this membership list?`
  String get circleWithMemberList {
    return Intl.message(
      'with this membership list?',
      name: 'circleWithMemberList',
      desc: '',
      args: [],
    );
  }

  /// `and invite this people?`
  String get circleInvitePeople {
    return Intl.message(
      'and invite this people?',
      name: 'circleInvitePeople',
      desc: '',
      args: [],
    );
  }

  /// `Select people to invite to your circle.`
  String get circleSelectPeople {
    return Intl.message(
      'Select people to invite to your circle.',
      name: 'circleSelectPeople',
      desc: '',
      args: [],
    );
  }

  /// `No ids selected`
  String get circleErrorNoIds {
    return Intl.message(
      'No ids selected',
      name: 'circleErrorNoIds',
      desc: '',
      args: [],
    );
  }

  /// `No contacts yet, wait for propagation.`
  String get circleErrorNoContactsYet {
    return Intl.message(
      'No contacts yet, wait for propagation.',
      name: 'circleErrorNoContactsYet',
      desc: '',
      args: [],
    );
  }

  /// `Circle name can't be empty.`
  String get circleErrorNameEmpty {
    return Intl.message(
      'Circle name can\'t be empty.',
      name: 'circleErrorNameEmpty',
      desc: '',
      args: [],
    );
  }

  /// `You have to select at least one person to add to the circle.`
  String get circleErrorSelectPerson {
    return Intl.message(
      'You have to select at least one person to add to the circle.',
      name: 'circleErrorSelectPerson',
      desc: '',
      args: [],
    );
  }

  /// `Select a circle`
  String get circleSelectCircle {
    return Intl.message(
      'Select a circle',
      name: 'circleSelectCircle',
      desc: '',
      args: [],
    );
  }

  /// `Accept Invite`
  String get circleAcceptInvite {
    return Intl.message(
      'Accept Invite',
      name: 'circleAcceptInvite',
      desc: '',
      args: [],
    );
  }

  /// `Information`
  String get circleCardTitle {
    return Intl.message(
      'Information',
      name: 'circleCardTitle',
      desc: '',
      args: [],
    );
  }

  /// `Leave or join a circle will update 20 or 30 seconds after accept or revoke the invite.`
  String get circleCardContent {
    return Intl.message(
      'Leave or join a circle will update 20 or 30 seconds after accept or revoke the invite.',
      name: 'circleCardContent',
      desc: '',
      args: [],
    );
  }

  /// `Creating circle`
  String get circleDetailsCreateCircle {
    return Intl.message(
      'Creating circle',
      name: 'circleDetailsCreateCircle',
      desc: '',
      args: [],
    );
  }

  /// `Updating circle details`
  String get circleDetailsUpdateCircle {
    return Intl.message(
      'Updating circle details',
      name: 'circleDetailsUpdateCircle',
      desc: '',
      args: [],
    );
  }

  /// `Finishing`
  String get circleDetailsFinishing {
    return Intl.message(
      'Finishing',
      name: 'circleDetailsFinishing',
      desc: '',
      args: [],
    );
  }

  /// `elRepo.io is Loading ...`
  String get elRepoIoIsLoading {
    return Intl.message(
      'elRepo.io is Loading ...',
      name: 'elRepoIoIsLoading',
      desc: '',
      args: [],
    );
  }

  /// `Publish`
  String get publish {
    return Intl.message(
      'Publish',
      name: 'publish',
      desc: '',
      args: [],
    );
  }

  /// `Browse`
  String get browse {
    return Intl.message(
      'Browse',
      name: 'browse',
      desc: '',
      args: [],
    );
  }

  /// `tags`
  String get tags {
    return Intl.message(
      'tags',
      name: 'tags',
      desc: '',
      args: [],
    );
  }

  /// `local\nrepo`
  String get localRepo {
    return Intl.message(
      'local\nrepo',
      name: 'localRepo',
      desc: '',
      args: [],
    );
  }

  /// `Sign up`
  String get signUp {
    return Intl.message(
      'Sign up',
      name: 'signUp',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get username {
    return Intl.message(
      'Username',
      name: 'username',
      desc: '',
      args: [],
    );
  }

  /// `Username cannot be empty.`
  String get usernameCannotBeEmpty {
    return Intl.message(
      'Username cannot be empty.',
      name: 'usernameCannotBeEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Password cannot be empty.`
  String get passwordCannotBeEmpty {
    return Intl.message(
      'Password cannot be empty.',
      name: 'passwordCannotBeEmpty',
      desc: '',
      args: [],
    );
  }

  /// `... creating account.\n This may take a while.`
  String get creatingAccount {
    return Intl.message(
      '... creating account.\n This may take a while.',
      name: 'creatingAccount',
      desc: '',
      args: [],
    );
  }

  /// `Create account`
  String get createAccount {
    return Intl.message(
      'Create account',
      name: 'createAccount',
      desc: '',
      args: [],
    );
  }

  /// `Logging in. This can take a while...`
  String get loggingIn {
    return Intl.message(
      'Logging in. This can take a while...',
      name: 'loggingIn',
      desc: '',
      args: [],
    );
  }

  /// `Login failed. Please retry.`
  String get loginFailed {
    return Intl.message(
      'Login failed. Please retry.',
      name: 'loginFailed',
      desc: '',
      args: [],
    );
  }

  /// `Log in`
  String get login {
    return Intl.message(
      'Log in',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Logging as`
  String get loggingInAs {
    return Intl.message(
      'Logging as',
      name: 'loggingInAs',
      desc: '',
      args: [],
    );
  }

  /// `Cannot load content`
  String get cannotLoadContent {
    return Intl.message(
      'Cannot load content',
      name: 'cannotLoadContent',
      desc: '',
      args: [],
    );
  }

  /// `Downloading content...`
  String get savingContentToLocalRepo {
    return Intl.message(
      'Downloading content...',
      name: 'savingContentToLocalRepo',
      desc: '',
      args: [],
    );
  }

  /// `Downloading content...`
  String get bookmarkContent {
    return Intl.message(
      'Downloading content...',
      name: 'bookmarkContent',
      desc: '',
      args: [],
    );
  }

  /// `Comment`
  String get comment {
    return Intl.message(
      'Comment',
      name: 'comment',
      desc: '',
      args: [],
    );
  }

  /// `title`
  String get title {
    return Intl.message(
      'title',
      name: 'title',
      desc: '',
      args: [],
    );
  }

  /// `Title cannot be empty.`
  String get titleCannotBeEmpty {
    return Intl.message(
      'Title cannot be empty.',
      name: 'titleCannotBeEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Description`
  String get description {
    return Intl.message(
      'Description',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Choose File`
  String get chooseFile {
    return Intl.message(
      'Choose File',
      name: 'chooseFile',
      desc: '',
      args: [],
    );
  }

  /// `Publishing content...\n If you have added heavy files this may take several minutes to complete.`
  String get publishingContent {
    return Intl.message(
      'Publishing content...\n If you have added heavy files this may take several minutes to complete.',
      name: 'publishingContent',
      desc: '',
      args: [],
    );
  }

  /// `Content published`
  String get contentPublished {
    return Intl.message(
      'Content published',
      name: 'contentPublished',
      desc: '',
      args: [],
    );
  }

  /// `Check it out`
  String get checkItOut {
    return Intl.message(
      'Check it out',
      name: 'checkItOut',
      desc: '',
      args: [],
    );
  }

  /// `Tip: you can add #HashTags in your description to make it easier to discover your post.`
  String get youCanAddHashTags {
    return Intl.message(
      'Tip: you can add #HashTags in your description to make it easier to discover your post.',
      name: 'youCanAddHashTags',
      desc: '',
      args: [],
    );
  }

  /// `posting comment`
  String get postingComment {
    return Intl.message(
      'posting comment',
      name: 'postingComment',
      desc: '',
      args: [],
    );
  }

  /// `Comment posted`
  String get commentPosted {
    return Intl.message(
      'Comment posted',
      name: 'commentPosted',
      desc: '',
      args: [],
    );
  }

  /// `follow`
  String get follow {
    return Intl.message(
      'follow',
      name: 'follow',
      desc: '',
      args: [],
    );
  }

  /// `discover`
  String get discover {
    return Intl.message(
      'discover',
      name: 'discover',
      desc: '',
      args: [],
    );
  }

  /// `following`
  String get following {
    return Intl.message(
      'following',
      name: 'following',
      desc: '',
      args: [],
    );
  }

  /// `Create and share backup file`
  String get createandshare {
    return Intl.message(
      'Create and share backup file',
      name: 'createandshare',
      desc: '',
      args: [],
    );
  }

  /// `Creating backup, please wait`
  String get creatingBackup {
    return Intl.message(
      'Creating backup, please wait',
      name: 'creatingBackup',
      desc: '',
      args: [],
    );
  }

  /// `Your account data will be copied to a zip file and you will be presented with a menu to share it.\n\n Choose a destination you consider secure and reliable.\n\nOnce the backup is shared you will need to log back in to elRepo.io\n\nIf you ever need to restore this backup, you will need to download the zip file\n\n and provide it in the sign-up screen.`
  String get backupText {
    return Intl.message(
      'Your account data will be copied to a zip file and you will be presented with a menu to share it.\n\n Choose a destination you consider secure and reliable.\n\nOnce the backup is shared you will need to log back in to elRepo.io\n\nIf you ever need to restore this backup, you will need to download the zip file\n\n and provide it in the sign-up screen.',
      name: 'backupText',
      desc: '',
      args: [],
    );
  }

  /// `I have a backup`
  String get haveABackup {
    return Intl.message(
      'I have a backup',
      name: 'haveABackup',
      desc: '',
      args: [],
    );
  }

  /// `Restoring backup`
  String get restoreBackup {
    return Intl.message(
      'Restoring backup',
      name: 'restoreBackup',
      desc: '',
      args: [],
    );
  }

  /// `Automatic peering`
  String get automaticpeering {
    return Intl.message(
      'Automatic peering',
      name: 'automaticpeering',
      desc: '',
      args: [],
    );
  }

  /// `If activated you will automatically connect with other elRepo.io nodes in your local network.`
  String get peeringdescription {
    return Intl.message(
      'If activated you will automatically connect with other elRepo.io nodes in your local network.',
      name: 'peeringdescription',
      desc: '',
      args: [],
    );
  }

  /// `Known nodes`
  String get knownnodes {
    return Intl.message(
      'Known nodes',
      name: 'knownnodes',
      desc: '',
      args: [],
    );
  }

  /// `Attempt connection`
  String get attemptconnection {
    return Intl.message(
      'Attempt connection',
      name: 'attemptconnection',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get delete {
    return Intl.message(
      'Delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `Remote Control`
  String get remoteControl {
    return Intl.message(
      'Remote Control',
      name: 'remoteControl',
      desc: '',
      args: [],
    );
  }

  /// `You need to specify the complete URL to your remote node control API.`
  String get remoteRules {
    return Intl.message(
      'You need to specify the complete URL to your remote node control API.',
      name: 'remoteRules',
      desc: '',
      args: [],
    );
  }

  /// `Connect`
  String get connect {
    return Intl.message(
      'Connect',
      name: 'connect',
      desc: '',
      args: [],
    );
  }

  /// `Disconnect`
  String get disconnect {
    return Intl.message(
      'Disconnect',
      name: 'disconnect',
      desc: '',
      args: [],
    );
  }

  /// `ERROR: Could not connect to remote node!`
  String get remoteControlerror {
    return Intl.message(
      'ERROR: Could not connect to remote node!',
      name: 'remoteControlerror',
      desc: '',
      args: [],
    );
  }

  /// `not connected to a remote elRepo.io node`
  String get remoteControlLocallyConnected {
    return Intl.message(
      'not connected to a remote elRepo.io node',
      name: 'remoteControlLocallyConnected',
      desc: '',
      args: [],
    );
  }

  /// `Connected to: {rsPrefix}`
  String remoteControlConnectedTo(Object rsPrefix) {
    return Intl.message(
      'Connected to: $rsPrefix',
      name: 'remoteControlConnectedTo',
      desc: '',
      args: [rsPrefix],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Your elRepo.io network`
  String get network {
    return Intl.message(
      'Your elRepo.io network',
      name: 'network',
      desc: '',
      args: [],
    );
  }

  /// `Create a Backup`
  String get backup {
    return Intl.message(
      'Create a Backup',
      name: 'backup',
      desc: '',
      args: [],
    );
  }

  /// `Choose a language`
  String get language {
    return Intl.message(
      'Choose a language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Remote Control`
  String get remoteCtrl {
    return Intl.message(
      'Remote Control',
      name: 'remoteCtrl',
      desc: '',
      args: [],
    );
  }

  /// `App intro`
  String get appIntro {
    return Intl.message(
      'App intro',
      name: 'appIntro',
      desc: '',
      args: [],
    );
  }

  /// `App settings`
  String get appSettings {
    return Intl.message(
      'App settings',
      name: 'appSettings',
      desc: '',
      args: [],
    );
  }

  /// `Dark Theme`
  String get darkMode {
    return Intl.message(
      'Dark Theme',
      name: 'darkMode',
      desc: '',
      args: [],
    );
  }

  /// `Receive notifications`
  String get notifications {
    return Intl.message(
      'Receive notifications',
      name: 'notifications',
      desc: '',
      args: [],
    );
  }

  /// `SIGN OUT`
  String get signOut {
    return Intl.message(
      'SIGN OUT',
      name: 'signOut',
      desc: '',
      args: [],
    );
  }

  /// `To manually add friends and connect to them on elRepo.io network, you need to do two actions:\n1. Share your elRepo.io id with your friends \n2. Get your friends ids and add it to your elRepo.io network\nRemember that both actions have to be done in order to become friends!`
  String get addFriendInstructions {
    return Intl.message(
      'To manually add friends and connect to them on elRepo.io network, you need to do two actions:\n1. Share your elRepo.io id with your friends \n2. Get your friends ids and add it to your elRepo.io network\nRemember that both actions have to be done in order to become friends!',
      name: 'addFriendInstructions',
      desc: '',
      args: [],
    );
  }

  /// `Your information`
  String get addFriendYourIdTitle {
    return Intl.message(
      'Your information',
      name: 'addFriendYourIdTitle',
      desc: '',
      args: [],
    );
  }

  /// `Send your elRepo.io invite to your friends so they can add you to their elRepo.io network`
  String get addFriendYourIdExplanation {
    return Intl.message(
      'Send your elRepo.io invite to your friends so they can add you to their elRepo.io network',
      name: 'addFriendYourIdExplanation',
      desc: '',
      args: [],
    );
  }

  /// `Add your friend's invite`
  String get addFriendPeerIdTitle {
    return Intl.message(
      'Add your friend\'s invite',
      name: 'addFriendPeerIdTitle',
      desc: '',
      args: [],
    );
  }

  /// `Save your friends to your elRepo.io network`
  String get addFriendPeerIdExplanation {
    return Intl.message(
      'Save your friends to your elRepo.io network',
      name: 'addFriendPeerIdExplanation',
      desc: '',
      args: [],
    );
  }

  /// `Add friend`
  String get addFriendButton {
    return Intl.message(
      'Add friend',
      name: 'addFriendButton',
      desc: '',
      args: [],
    );
  }

  /// `Paste the invite here`
  String get addFriendHint {
    return Intl.message(
      'Paste the invite here',
      name: 'addFriendHint',
      desc: '',
      args: [],
    );
  }

  /// `Synchronizing posts`
  String get commonButtonsSynchronizePosts {
    return Intl.message(
      'Synchronizing posts',
      name: 'commonButtonsSynchronizePosts',
      desc: '',
      args: [],
    );
  }

  /// `Loading file info...`
  String get fileDownloadCardLoadingInfo {
    return Intl.message(
      'Loading file info...',
      name: 'fileDownloadCardLoadingInfo',
      desc: '',
      args: [],
    );
  }

  /// `Hashing...`
  String get fileDownloadCardHashing {
    return Intl.message(
      'Hashing...',
      name: 'fileDownloadCardHashing',
      desc: '',
      args: [],
    );
  }

  /// `Delete file`
  String get fileDownloadDeleteFile {
    return Intl.message(
      'Delete file',
      name: 'fileDownloadDeleteFile',
      desc: '',
      args: [],
    );
  }

  /// `This action will delete the file from your file  system. Are you sure to do that?`
  String get fileDownloadDeleteFileConfirmation {
    return Intl.message(
      'This action will delete the file from your file  system. Are you sure to do that?',
      name: 'fileDownloadDeleteFileConfirmation',
      desc: '',
      args: [],
    );
  }

  /// `Cancel download`
  String get fileDownloadCardCancelDownload {
    return Intl.message(
      'Cancel download',
      name: 'fileDownloadCardCancelDownload',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure that you want to cancel this download?`
  String get fileDownloadCardCancelDownloadConfirmation {
    return Intl.message(
      'Are you sure that you want to cancel this download?',
      name: 'fileDownloadCardCancelDownloadConfirmation',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'it'),
      Locale.fromSubtags(languageCode: 'pt'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}
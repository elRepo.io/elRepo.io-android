// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pt locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pt';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appIntro" : MessageLookupByLibrary.simpleMessage("App intro"),
    "appSettings" : MessageLookupByLibrary.simpleMessage("Configurações do app"),
    "attemptconnection" : MessageLookupByLibrary.simpleMessage("Tentativa de conexão"),
    "automaticpeering" : MessageLookupByLibrary.simpleMessage("Peering automático"),
    "backup" : MessageLookupByLibrary.simpleMessage("Criar um backup"),
    "backupText" : MessageLookupByLibrary.simpleMessage("Os dados da sua conta serão copiados para um arquivo zip e será apresentado um menu para compartilhá-los.\n\nEscolha um destino que considere seguro e confiável.\n\nQuando o backup for compartilhado, você irá precisa fazer login novamente em elRepo.io\n\nSe precisar restaurar este backup, você precisará baixar o arquivo zip\n\ne fornecê-lo na tela de inscrição."),
    "browse" : MessageLookupByLibrary.simpleMessage("Navegar"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancelar"),
    "cannotLoadContent" : MessageLookupByLibrary.simpleMessage("Não é possível carregar o conteúdo."),
    "checkItOut" : MessageLookupByLibrary.simpleMessage("Confira"),
    "chooseFile" : MessageLookupByLibrary.simpleMessage("Escolher arquivo."),
    "circle" : MessageLookupByLibrary.simpleMessage("Círculo"),
    "circleErrorNameEmpty" : MessageLookupByLibrary.simpleMessage("O nome do círculo não pode estar vazio."),
    "circleErrorNoContactsYet" : MessageLookupByLibrary.simpleMessage("Nenhum contato ainda, aguarde a propagação."),
    "circleErrorNoIds" : MessageLookupByLibrary.simpleMessage("Nenhum id selecionado."),
    "circleErrorSelectPerson" : MessageLookupByLibrary.simpleMessage("Você deve selecionar pelo menos uma pessoa para adicionar ao círculo."),
    "circleInvitePeople" : MessageLookupByLibrary.simpleMessage("E convidar esta pessoa?"),
    "circleSelectPeople" : MessageLookupByLibrary.simpleMessage("Selecione as pessoas para convidar para o seu círculo."),
    "circleWithMemberList" : MessageLookupByLibrary.simpleMessage("Com esta lista de membros?"),
    "circlesTitle" : MessageLookupByLibrary.simpleMessage("Círculos"),
    "comment" : MessageLookupByLibrary.simpleMessage("Comentário"),
    "commentPosted" : MessageLookupByLibrary.simpleMessage("Comentário postado"),
    "connect" : MessageLookupByLibrary.simpleMessage("Conectar"),
    "cont" : MessageLookupByLibrary.simpleMessage("Continue"),
    "contentPublished" : MessageLookupByLibrary.simpleMessage("Conteúdo publicado"),
    "create" : MessageLookupByLibrary.simpleMessage("Criar"),
    "createAccount" : MessageLookupByLibrary.simpleMessage("Criar conta"),
    "createandshare" : MessageLookupByLibrary.simpleMessage("Criar e compartilhar arquivo de backup"),
    "creatingAccount" : MessageLookupByLibrary.simpleMessage("... criando uma conta. \n Isso pode demorar um pouco."),
    "darkMode" : MessageLookupByLibrary.simpleMessage("Tema escuro"),
    "delete" : MessageLookupByLibrary.simpleMessage("Remover"),
    "description" : MessageLookupByLibrary.simpleMessage("Descrição"),
    "details" : MessageLookupByLibrary.simpleMessage("Detalhes"),
    "disconnect" : MessageLookupByLibrary.simpleMessage("Desconectar"),
    "discover" : MessageLookupByLibrary.simpleMessage("Descubrir"),
    "edit" : MessageLookupByLibrary.simpleMessage("Editar"),
    "elRepoIoIsLoading" : MessageLookupByLibrary.simpleMessage("elRepo.io está carregando ..."),
    "error" : MessageLookupByLibrary.simpleMessage("Erro"),
    "follow" : MessageLookupByLibrary.simpleMessage("Siga"),
    "following" : MessageLookupByLibrary.simpleMessage("Seguindo"),
    "knownnodes" : MessageLookupByLibrary.simpleMessage("Nós conhecidos"),
    "language" : MessageLookupByLibrary.simpleMessage("Escolha um idioma"),
    "leave" : MessageLookupByLibrary.simpleMessage("Sair"),
    "localRepo" : MessageLookupByLibrary.simpleMessage("local\nrepo"),
    "loggingIn" : MessageLookupByLibrary.simpleMessage("Fazendo login. Isso pode demorar um pouco ..."),
    "loggingInAs" : MessageLookupByLibrary.simpleMessage("Registrando como"),
    "login" : MessageLookupByLibrary.simpleMessage("Entrar"),
    "loginFailed" : MessageLookupByLibrary.simpleMessage("Falha no login. Tente novamente."),
    "name" : MessageLookupByLibrary.simpleMessage("Nome"),
    "network" : MessageLookupByLibrary.simpleMessage("Sua rede elRepo.io"),
    "notifications" : MessageLookupByLibrary.simpleMessage("Receber notificações"),
    "participants" : MessageLookupByLibrary.simpleMessage("Participantes"),
    "password" : MessageLookupByLibrary.simpleMessage("Senha"),
    "passwordCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("A senha não pode estar vazia."),
    "peeringdescription" : MessageLookupByLibrary.simpleMessage("Se ativado, você se conectará automaticamente a outros nós elRepo.io em sua rede local."),
    "postingComment" : MessageLookupByLibrary.simpleMessage("Postando comentário"),
    "publish" : MessageLookupByLibrary.simpleMessage("Publicar"),
    "publishingContent" : MessageLookupByLibrary.simpleMessage("Publicando conteúdo ... \n Se você adicionou arquivos pesados, a conclusão pode demorar vários minutos."),
    "remoteControl" : MessageLookupByLibrary.simpleMessage("Controle remoto"),
    "remoteControlerror" : MessageLookupByLibrary.simpleMessage("ERRO: Não foi possível conectar ao nó remoto!"),
    "remoteCtrl" : MessageLookupByLibrary.simpleMessage("Controle remoto"),
    "remoteRules" : MessageLookupByLibrary.simpleMessage("Você precisa especificar o URL completo para sua API de controle de nó remoto."),
    "savingContentToLocalRepo" : MessageLookupByLibrary.simpleMessage("Baixando conteúdo..."),
    "settings" : MessageLookupByLibrary.simpleMessage("Configurações"),
    "signOut" : MessageLookupByLibrary.simpleMessage("SIGN OUT"),
    "signUp" : MessageLookupByLibrary.simpleMessage("Registrar-se"),
    "tags" : MessageLookupByLibrary.simpleMessage("tags"),
    "title" : MessageLookupByLibrary.simpleMessage("Título"),
    "titleCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("O título não pode estar vazio."),
    "username" : MessageLookupByLibrary.simpleMessage("Nome de usuário"),
    "usernameCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("O nome de usuário não pode estar vazio."),
    "youCanAddHashTags" : MessageLookupByLibrary.simpleMessage("Dica: você pode adicionar #HashTags em sua descrição para facilitar a descoberta de sua postagem."),
    "your" : MessageLookupByLibrary.simpleMessage("Seu")
  };
}

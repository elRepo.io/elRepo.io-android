// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appIntro" : MessageLookupByLibrary.simpleMessage("Introducción de la aplicación"),
    "appSettings" : MessageLookupByLibrary.simpleMessage("Configuración de la aplicación"),
    "attemptconnection" : MessageLookupByLibrary.simpleMessage("Intento de conexión"),
    "automaticpeering" : MessageLookupByLibrary.simpleMessage("Emparejamiento automático"),
    "backup" : MessageLookupByLibrary.simpleMessage("Crea una copia de seguridad"),
    "backupText" : MessageLookupByLibrary.simpleMessage("Los datos de tu cuenta se copiarán en un archivo zip y se mostrará un menú para compartirlos \n\nElije un destino seguro y confiable\n\nUna vez que se comparte la copia de seguridad, deberás volver a iniciar sesión en elRepo.io\n\nSi alguna vez necesitas restaurar esta copia de seguridad, deberás descargar el archivo zip\n\nY cargarlo en la pantalla de registro"),
    "browse" : MessageLookupByLibrary.simpleMessage("Explorar"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancelar"),
    "cannotLoadContent" : MessageLookupByLibrary.simpleMessage("No se puede cargar el contenido"),
    "checkItOut" : MessageLookupByLibrary.simpleMessage("Visitarlo"),
    "chooseFile" : MessageLookupByLibrary.simpleMessage("Elegir archivo"),
    "circle" : MessageLookupByLibrary.simpleMessage("Círculo"),
    "circleErrorNameEmpty" : MessageLookupByLibrary.simpleMessage("El nombre del Círculo no puede estar vacío."),
    "circleErrorNoContactsYet" : MessageLookupByLibrary.simpleMessage("Aún no hay contactos, espera la propagación"),
    "circleErrorNoIds" : MessageLookupByLibrary.simpleMessage("No hay contactos seleccionados."),
    "circleErrorSelectPerson" : MessageLookupByLibrary.simpleMessage("Tienes que seleccionar al menos una persona para agregar al círculo."),
    "circleInvitePeople" : MessageLookupByLibrary.simpleMessage("e invitar a estas personas?"),
    "circleSelectPeople" : MessageLookupByLibrary.simpleMessage("Selecciona las personar a invitar a tu círculo."),
    "circleWithMemberList" : MessageLookupByLibrary.simpleMessage("con esta lista de miembros?"),
    "circlesTitle" : MessageLookupByLibrary.simpleMessage("Círculos"),
    "comment" : MessageLookupByLibrary.simpleMessage("Comentar"),
    "commentPosted" : MessageLookupByLibrary.simpleMessage("Comentario enviado"),
    "connect" : MessageLookupByLibrary.simpleMessage("Conecta"),
    "cont" : MessageLookupByLibrary.simpleMessage("Continuar"),
    "contentPublished" : MessageLookupByLibrary.simpleMessage("Contenido publicado"),
    "create" : MessageLookupByLibrary.simpleMessage("Crear"),
    "createAccount" : MessageLookupByLibrary.simpleMessage("Crear cuenta"),
    "createandshare" : MessageLookupByLibrary.simpleMessage("Crea y comparte una copia de seguridad"),
    "creatingAccount" : MessageLookupByLibrary.simpleMessage("... creando cuenta.\n Esto puede llevar varios minutos"),
    "darkMode" : MessageLookupByLibrary.simpleMessage("Tema oscuro"),
    "delete" : MessageLookupByLibrary.simpleMessage("Eliminar"),
    "description" : MessageLookupByLibrary.simpleMessage("Descripción"),
    "details" : MessageLookupByLibrary.simpleMessage("Detalles"),
    "disconnect" : MessageLookupByLibrary.simpleMessage("Desconecta"),
    "discover" : MessageLookupByLibrary.simpleMessage("Descubrir"),
    "edit" : MessageLookupByLibrary.simpleMessage("Editar"),
    "elRepoIoIsLoading" : MessageLookupByLibrary.simpleMessage("elRepo.io está cargando ..."),
    "error" : MessageLookupByLibrary.simpleMessage("Error"),
    "follow" : MessageLookupByLibrary.simpleMessage("Seguir"),
    "following" : MessageLookupByLibrary.simpleMessage("Siguiendo"),
    "haveABackup" : MessageLookupByLibrary.simpleMessage("Tengo un backup"),
    "knownnodes" : MessageLookupByLibrary.simpleMessage("Nodos conocidos"),
    "language" : MessageLookupByLibrary.simpleMessage("Elige un idioma"),
    "leave" : MessageLookupByLibrary.simpleMessage("Abandonar"),
    "localRepo" : MessageLookupByLibrary.simpleMessage("repo\nlocal"),
    "loggingIn" : MessageLookupByLibrary.simpleMessage("Ingresando. Esto puede llevar un tiempo..."),
    "loggingInAs" : MessageLookupByLibrary.simpleMessage("Ingresando como"),
    "login" : MessageLookupByLibrary.simpleMessage("Ingresar"),
    "loginFailed" : MessageLookupByLibrary.simpleMessage("El ingreso falló. Por favor reintente."),
    "name" : MessageLookupByLibrary.simpleMessage("Nombre"),
    "network" : MessageLookupByLibrary.simpleMessage("Tu red de elRepo.io"),
    "notifications" : MessageLookupByLibrary.simpleMessage("Recibir notificaciones"),
    "participants" : MessageLookupByLibrary.simpleMessage("Participantes"),
    "password" : MessageLookupByLibrary.simpleMessage("Contraseña"),
    "passwordCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("La clave no puede estar vacía"),
    "peeringdescription" : MessageLookupByLibrary.simpleMessage("Si activas esta opción, te conectarás automáticamente con otros nodos de elRepo.io en tu red local"),
    "postingComment" : MessageLookupByLibrary.simpleMessage("Publicando comentario"),
    "publish" : MessageLookupByLibrary.simpleMessage("Publicar"),
    "publishingContent" : MessageLookupByLibrary.simpleMessage("Publicando contenido...\n Si se agregaron archivos pesados el proceso puede demorar varios minutos."),
    "remoteControl" : MessageLookupByLibrary.simpleMessage("Control Remoto"),
    "remoteControlerror" : MessageLookupByLibrary.simpleMessage("ERROR: No se pudo conectar al nodo remoto!"),
    "remoteCtrl" : MessageLookupByLibrary.simpleMessage("Control remoto"),
    "remoteRules" : MessageLookupByLibrary.simpleMessage("Debes especificar la URL completa de tu API de control de nodos remotos"),
    "savingContentToLocalRepo" : MessageLookupByLibrary.simpleMessage("Descargando contenido..."),
    "settings" : MessageLookupByLibrary.simpleMessage("Ajustes"),
    "signOut" : MessageLookupByLibrary.simpleMessage("Cerrar Sesión"),
    "signUp" : MessageLookupByLibrary.simpleMessage("Crear cuenta"),
    "tags" : MessageLookupByLibrary.simpleMessage("tags"),
    "title" : MessageLookupByLibrary.simpleMessage("Título"),
    "titleCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("el título no puede estar vacío"),
    "username" : MessageLookupByLibrary.simpleMessage("Nombre o apodo"),
    "usernameCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("El nombre no puede estar vacío"),
    "youCanAddHashTags" : MessageLookupByLibrary.simpleMessage("Si agregas #HashTags en la descripción será más fácil que otros descubran tu publicación."),
    "your" : MessageLookupByLibrary.simpleMessage("Tus")
  };
}

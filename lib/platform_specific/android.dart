/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';

import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart';
import 'package:elRepoIo/routes.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:elrepo_lib/constants.dart' as cnst show AccountStatus;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs show isRetroshareRunning;
import 'package:elrepo_lib/repo.dart' as repo show getAccountStatus, prepareSystem;
import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rsControl;

/// Sharing intents are used to recieve data from other apps, like with the
/// typical button of: "open with" or "share with"
class HandleSharingIntent {
  /// Stream subscription for sharing intents
  StreamSubscription intentDataStreamSubscription;

  /// Used when receive a sharing intent to navigate to publish screen without
  /// using context
  final GlobalKey<NavigatorState> navigatorKey;

  HandleSharingIntent(this.navigatorKey);

  /// Redirect to publish tab when sharing intent is received
  Future<void> redirectToPublishWithArgs (TabsPageArguments args) async {
    navigatorKey.currentState.pushReplacementNamed(
        '/main',
        // todo(kon): this is pretty ugly and non scalable, we should use a state provider instead
        arguments: args
    );
  }

  /// Function executed when a text shared intent is received
  Future<void> openSharedIntentText(String text) async {
    if(text != null && text.isNotEmpty) {
      // todo(kon): If RS is not runing or not logged in, do the stuff and then redirect with the data
      if (await rs.isRetroshareRunning()
          && await repo.getAccountStatus() == cnst.AccountStatus.isLoggedIn) {
        if (text.contains('retroshare.me/addfriend?rsInvite=')) {
          // Do nothing, this is handled by route
        } else {
          redirectToPublishWithArgs(TabsPageArguments(
              publishFormArgs: PublishFormArgs(descriptionText: text)
          ));
        }
      }
    }
  }

  // Redirect to publish with the arguments of a received file path to share
  void openSharedIntentFile(String filePath) {
    if(filePath != null && filePath.isNotEmpty) {
      redirectToPublishWithArgs(TabsPageArguments(
          publishFormArgs: PublishFormArgs(selectedFilePath: filePath)
      ));
    }
  }

  // Function that subscribe different stream subscriptions
  void startReceiveSharingIntent() {
    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) {
          print('Receiving text sharing intent - while app on memory : $value');
          openSharedIntentText(value);
        }, onError: (err) {
          print("getLinkStream error: $err");
        });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((String value) {
      print('Receiving text sharing intent - while app closed : $value');
      openSharedIntentText(value);
    });

    // For sharing images coming from outside the app while the app is in the memory
    intentDataStreamSubscription =
        ReceiveSharingIntent.getMediaStream().listen((List<SharedMediaFile> value) {
          // print('Receiving file sharing intent - while app on memory : ${(value?.map((f)=> f.path)?.join(",") ?? "")}');
          print('Receiving file sharing intent - while app on memory : ${value[0].path}');
          if (value.isNotEmpty) openSharedIntentFile(value[0].path);
        }, onError: (err) {
          print("getIntentDataStream error: $err");
        });

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia().then((List<SharedMediaFile> value) {
      print('Receiving file sharing intent - while app closed : ${(value?.map((f)=> f.path)?.join(",") ?? "")}');
      if (value.isNotEmpty) openSharedIntentFile(value[0].path);
    });
  }
}
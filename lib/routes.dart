/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/ui/main_page/models/browse_tab_bar.dart';
import 'package:elRepoIo/ui/browse_tag.dart';
import 'package:elRepoIo/ui/main_page/main_screen.dart';
import 'package:elRepoIo/ui/circles/show_circles.dart';
import 'package:elRepoIo/ui/circles/circle_details.dart';
import 'package:elRepoIo/ui/common/no_data_yet.dart';
import 'package:elRepoIo/ui/contact_admin.dart';
import 'package:elRepoIo/ui/create_backup.dart';
import 'package:elRepoIo/ui/edit_profile.dart';
import 'package:elRepoIo/ui/friends/add_friend.dart';
import 'package:elRepoIo/ui/friends/friend_nodes.dart';
import 'package:elRepoIo/ui/identity_details.dart';
import 'package:elRepoIo/ui/login.dart';
import 'package:elRepoIo/ui/onboarding.dart';
import 'package:elRepoIo/ui/post_details_screen/post_details_screen.dart';
import 'package:elRepoIo/ui/remote_control.dart';
import 'package:elRepoIo/ui/search.dart';
import 'package:elRepoIo/ui/settings.dart';
import 'package:elRepoIo/ui/signup.dart';
import 'package:elRepoIo/ui/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/constants.dart' as cnst;
import 'package:retroshare_dart_wrapper/rs_models.dart' show Identity, RsMsgMetaData;

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    var name = settings.name;
    var arguments = settings.arguments;
    Uri uriParsed;
    // Check for settings.name parameters after ? used on deep linking
    // Todo: use a better way to do this, following this tutorial https://www.raywenderlich.com/19457817-flutter-navigator-2-0-and-deep-links
    if (settings.name.contains('?')) {
      uriParsed = Uri.parse(settings.name);
      name = uriParsed.path;
    }

    switch (name) {
      case '/':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginScreen(settings.arguments));
      case '/signup':
        return MaterialPageRoute(builder: (_) => SignUpScreen());
      case '/onboarding':
        return MaterialPageRoute(builder: (_) => OnBoarding());
      case '/nodatayet':
        return MaterialPageRoute(builder: (_) => NoDataYetPage());
      case '/friendnodes':
        return MaterialPageRoute(builder: (_) => FriendNodesScreen());
      case '/addfriend':
        if (uriParsed != null) {
          try {
            arguments = AddFriendArguments.fromUri(uriParsed);
          } catch (e) {
            print(e);
            return _errorRoute();
          }
        }
        return MaterialPageRoute(
            builder: (_) => AddFriendsScreen(arguments: arguments));
      case '/browsex':
        return MaterialPageRoute(builder: (_) => BrowseXScreen());
      case '/createbackup':
        return MaterialPageRoute(builder: (_) => BackupDownloading());
      case '/settings':
        return MaterialPageRoute(builder: (_) => SettingsProfile());
      case '/editprofile':
        return MaterialPageRoute(
            builder: (_) => EditMyProfile(settings.arguments));
      case '/contactadmin':
        return MaterialPageRoute(builder: (_) => ContactAdmin());
      case '/showCircles':
        return MaterialPageRoute(builder: (_) => CircleScreen());
      case '/remotecontrol':
        return MaterialPageRoute(builder: (_) => RemoteControl());
      case '/main':
        return settings.arguments is TabsPageArguments
            ? MaterialPageRoute(builder: (_) => MainScreen(tabsPageArguments: settings.arguments,))
            : MaterialPageRoute(builder: (_) => MainScreen( )) ;
    case '/postdetails':
        return MaterialPageRoute(
            builder: (_) => PublicationPage(settings.arguments));
      case '/browsetag':
        // Check if is an uri parsed parameter
        if (uriParsed != null) {
          try {
            arguments =
                TagArguments.fromUriQueryParameters(uriParsed.queryParameters);
          } catch (e) {
            print(e);
            return _errorRoute();
          }
        }
        return MaterialPageRoute(builder: (_) => BrowseTagScreen(arguments));
      case '/identityDetails':
        return MaterialPageRoute(
            builder: (_) => IdentityDetailsScreen(settings.arguments));
      case '/circleDetails':
        return MaterialPageRoute(
            builder: (_) => CircleDetailsScreen(settings.arguments));
      case '/search':
        return MaterialPageRoute(
            builder: (_) => SearchScreen(settings.arguments));
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Error'),
          ),
          body: Center(
            child: Text('Error'),
          ),
        );
      },
    );
  }
}

/// Function used tot pop until login, but with a previous step to dispose all
/// timers
// todo(kon): use global timer handler
void popUntilLogin(BuildContext context, {Map args}){
  Navigator.of(context).popUntil((route) => route.isFirst); // Do this to dispose all timers
  Navigator.pushReplacementNamed(context, '/login', arguments: rs.defaultLocation);
}

void redirectOnAccountStatus(
  BuildContext context,
) async {
  final accountStatus = await repo.getAccountStatus();
  switch (accountStatus) {
    case cnst.AccountStatus.isLoggedIn:
      Navigator.pushReplacementNamed(context, '/main');
      break;
    case cnst.AccountStatus.hasLocation:
      Navigator.pushReplacementNamed(context, '/login');
      break;
    case cnst.AccountStatus.hasNoLocation:
      Navigator.pushReplacementNamed(context, '/signup');
      break;
  }
}

/// DRY function for open circle details screen
void openCircleDetails(context, data) {
  Navigator.pushNamed(context, '/circleDetails',
      arguments: CircleArguments(
          true, data, '${S.of(context).circle} ${S.of(context).details}', ''));
}

class PostArguments {
  final String forumId;
  final String postId;

  /// Optional parameter with the already download postmetadata object
  final RsMsgMetaData postMetadata;
  PostArguments(this.forumId, this.postId, {this.postMetadata});
}

class TagArguments {
  final String tagName;
  final RsMsgMetaData postMetadata;
  TagArguments(this.tagName, [this.postMetadata]);
  factory TagArguments.fromUriQueryParameters(Map<String, String> params) {
    if (params['tagName'] != null) return TagArguments(params['tagName']);
    throw Exception('Bad query parameters for TagArguments: $params');
  }
}

class IdentityArguments {
  /// Identity gxs id
  final String identityId;

  /// The details already asked to Rs, to don't do the API call again asking for details
  final Identity idDetails;
  IdentityArguments(this.identityId, [this.idDetails]);
}

class CircleArguments {
  bool detailsOnly = false;
  // Result of /getCirclesInfo
  Map<String, dynamic> circleDetail = <String, dynamic>{};
  // Title and subtitle of the appbar
  String title, subtitle;
  CircleArguments(
      this.detailsOnly, this.circleDetail, this.title, this.subtitle);
}

class SearchArguments {
  // todo(kon): migrate this to a RsObject or change to only pass the parameter you need.
  // This is not enough generic args.
  final Map<String, dynamic> fileInfo;
  SearchArguments(this.fileInfo);
}

class AddFriendArguments {
  final String rsInvite;
  AddFriendArguments(this.rsInvite);
  factory AddFriendArguments.fromUri(Uri uri) {
    if ((uri.queryParameters)['rsInvite'] != null) {
      return AddFriendArguments('https://retroshare.me${uri.toString()}');
    }
    throw Exception('Bad query parameters for AddFriendArguments: ${uri.queryParameters}');
  }
}

/// Arguments for profile screen
///
/// They contain the information got from [repo.getOwnIdentityDetails()],
/// information related to the avatar, id name etc...
class EditProfileArguments {
  bool onlyAvatar; // Used to show only avatar edition, for example during the signup
  EditProfileArguments({this.onlyAvatar = false});
}

/// Used to pass data to the main screen tabs page
class TabsPageArguments {
  /// Select what tab have to be opened by default
  int tabIndex;
  /// Arguments for publish tab
  PublishFormArgs publishFormArgs;
  TabsPageArguments ({this.tabIndex, this.publishFormArgs});
}

class PublishFormArgs {
  /// Initial description text for publish tab
  String descriptionText;
  /// File path selected, sent from a receiving intent
  String selectedFilePath;
  PublishFormArgs ({this.descriptionText, this.selectedFilePath});
}

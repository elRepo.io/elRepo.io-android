/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:flutter/material.dart';

final BACKGROUND_COLOR = Color(0xffe5ffe4);
final WHITE_COLOR = Color(0xffffffff);
const LOCAL_COLOR = Color(0xffa5c461);
const ACCENT_COLOR = Color(0xffb3cd79);
const REMOTE_COLOR = Color(0xff895978);
const RED_COLOR = Color(0xff8a3444);
const PURPLE_COLOR = Color(0xff895978);
const NEUTRAL_COLOR = Color(0xff898989);
const BTN_FONT_COLOR = Color(0xff101b00);
const WARNING_COLOR = Color(0xffffD50A);

EdgeInsets defaultMargin() {
  return EdgeInsets.all(15.0);
}

/// Local storage database name
final STORAGE_NAME = 'local_preferences';
final SENTRY_SEND_REPORTS = 'sentry_send_reports';
final PREFERRED_LANGUAGE = 'preferred_language';
final ACTIVATE_PERCEPTUAL_SEARCH = 'perceptual_search';
final RS_CREDENTIALS = 'rs_credentials';

/// Icons
final ICON_BOOKMARKED_TRUE = Icons.download_rounded;
final ICON_BOOKMARKED_FALSE = Icons.download_outlined;
final ICON_YOUR_CONTENT = Icons.home;
const PUBLISH_ICON = Icons.add_circle_rounded;
const DISCOVER_ICON = Icons.explore;
